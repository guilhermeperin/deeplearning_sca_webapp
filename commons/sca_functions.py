import numpy as np


class ScaFunctions:

    def key_rank(self, probabilities, labels_key_hypothesis, number_of_key_hypothesis, good_key):

        key_rank = np.zeros(len(probabilities))
        key_probabilities = np.zeros(number_of_key_hypothesis)

        for index in range(len(probabilities)):
            probabilities_kg_ity_test = probabilities[index][
                np.asarray([int(leakage[index]) for leakage in labels_key_hypothesis[:]])]
            key_probabilities += np.log(probabilities_kg_ity_test)

            key_probabilities_sorted = np.argsort(key_probabilities)[::-1]

            for kg in range(number_of_key_hypothesis):

                if key_probabilities_sorted[kg] == good_key:
                    key_rank[index] = kg + 1
