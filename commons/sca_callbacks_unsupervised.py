from keras.callbacks import Callback
import numpy as np


class TrainingCallback(Callback):
    def __init__(self, x_data, y_data, number_of_epochs):
        self.current_epoch = 0
        self.x = x_data
        self.y = y_data
        self.accuracy = np.zeros((256, number_of_epochs))
        self.recall = np.zeros((256, number_of_epochs))
        self.loss = np.zeros((256, number_of_epochs))

    def on_epoch_end(self, epoch, logs={}):
        results = self.model.evaluate(self.x, self.y, verbose=0)

        for i in range(256):
            self.accuracy[i][epoch] = results[256 + 2 * i + 1]
            self.recall[i][epoch] = results[256 + 2 * i + 2]
            self.loss[i][epoch] = results[i + 1]

    def get_accuracy(self):
        return self.accuracy

    def get_recall(self):
        return self.recall

    def get_loss(self):
        return self.loss


class ValidationCallback(Callback):
    def __init__(self, x_data, y_data, number_of_epochs):
        self.current_epoch = 0
        self.x = x_data
        self.y = y_data
        self.accuracy = np.zeros((256, number_of_epochs))
        self.recall = np.zeros((256, number_of_epochs))
        self.loss = np.zeros((256, number_of_epochs))

    def on_epoch_end(self, epoch, logs={}):
        results = self.model.evaluate(self.x, self.y, verbose=0)

        for i in range(256):
            self.accuracy[i][epoch] = results[256 + 2 * i + 1]
            self.recall[i][epoch] = results[256 + 2 * i + 2]
            self.loss[i][epoch] = results[i + 1]

    def get_accuracy(self):
        return self.accuracy

    def get_recall(self):
        return self.recall

    def get_loss(self):
        return self.loss
