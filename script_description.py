class ScriptDescription:

    @staticmethod
    def script_descriptions():
        return {
            "script_single": "Run training for a single key byte",
            "script_single_all_keys": "Run training for all key bytes",
            "script_single_ensemble": "Run multiple trainings for single key byte and create an ensemble ",
            "script_single_ensemble_best_epoch_metrics": "Run multiple trainings for single key byte and create an "
                                                          "ensemble based on specific metrics",
            "script_multiple": "Run training for a single key byte multiple times and return succes rate and guessing "
                               "entropy",
            "script_multiple_all_keys": "Run training for all key bytes multiple times and return succes rate and "
                                        "guessing entropy",
            "script_multiple_hyper_parameters": "Run multiple trainings for a single key byte on different "
                                                "hyper-parameters",
            "script_single_best_epoch_metrics": "Run training for a single key byte and return key ranking based on "
                                                "best on specific metrics",
            'script_single_all_keys_best_epoch_metrics': "Run training for all key bytes and return key ranking based "
                                                         "on best on specific metrics",
            "script_multiple_best_epoch_metrics": "Run training for a single key byte multiple times and return key "
                                                  "ranking based on best on specific metrics",
            "script_multiple_all_keys_best_epoch_metrics": "Run training for all key bytes multiple times and return "
                                                           "key ranking based on best on specific metrics",
            "script_single_data_augmentation": "Run training for a single key byte with custom data augmentation methods"
        }
