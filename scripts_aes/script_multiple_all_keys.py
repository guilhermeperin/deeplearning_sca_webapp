import numpy as np
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import KeyRankingCallbackTrainingEnd
from commons.sca_datasets import ScaDataSets
from database.database_inserts import DatabaseInserts
from commons.sca_plots import Plots
import matplotlib.colors as colors
import matplotlib.cm as cmx
import pandas as pd
import time

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'D:/traces/'
sca_parameters = ScaParameters()
param = sca_parameters.get_trace_set("ches_ctf")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 0-10
#  "round_state_output": 0-10
#  "state_input": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  "state_output": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW, HD, ID, bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#

aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "HW",
    "bit": 0,
    "byte": 0,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#
data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, _, _, _, _ = data_sets.load_training_and_validation_sets()
x_test, trace_data_test, _ = data_sets.load_test_set()

# ---------------------------------------------------------------------------------------------------------------------#
#  Obtain test key as an int array
# ---------------------------------------------------------------------------------------------------------------------#
key_int = ([int(x) for x in bytearray.fromhex(param["key"])])

# ---------------------------------------------------------------------------------------------------------------------#
#  Loop for the number of training runs (for success rate calculation)
# ---------------------------------------------------------------------------------------------------------------------#
number_of_runs = 3

number_of_target_key_bytes = len(key_int)
number_of_traces_test = param["n_test"]
key_rank_test_all_keys = np.zeros((number_of_target_key_bytes, number_of_runs, number_of_traces_test))
guessing_entropy_all_keys = np.zeros((number_of_target_key_bytes, number_of_traces_test))
success_rate_all_keys = np.zeros((number_of_target_key_bytes, number_of_traces_test))

accuracy_training = np.zeros((number_of_target_key_bytes, param["epochs"]))
accuracy_validation = np.zeros((number_of_target_key_bytes, param["epochs"]))
recall_training = np.zeros((number_of_target_key_bytes, param["epochs"]))
recall_validation = np.zeros((number_of_target_key_bytes, param["epochs"]))
loss_training = np.zeros((number_of_target_key_bytes, param["epochs"]))
loss_validation = np.zeros((number_of_target_key_bytes, param["epochs"]))

first_key_byte = True
database_inserts = None
model_name = ""
model = None
learning_rate = 0
optimizer = ""
start = time.time()

# ---------------------------------------------------------------------------------------------------------------------#
#  Run train, validation and test for all target key bytes
# ---------------------------------------------------------------------------------------------------------------------#

for key_byte_index in range(number_of_target_key_bytes):

    print("Key Byte: " + str(key_byte_index))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Update leakage model and target key byte information
    # -----------------------------------------------------------------------------------------------------------------#

    param["good_key"] = key_int[key_byte_index]
    aes_leakage_model["byte"] = key_byte_index

    # ---------------------------------------------------------------------------------------------------------------------#
    #  Create categorical labels from selected leakage model and cipher
    #  A categorical label is a vector with all zeros, except at the index of the class/label
    #  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
    # ---------------------------------------------------------------------------------------------------------------------#
    crypto = AES()
    train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
    validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
    test_labels = crypto.create_labels(trace_data_test, aes_leakage_model, param)

    y_train = to_categorical(train_labels, num_classes=param["classes"])
    y_validation = to_categorical(validation_labels, num_classes=param["classes"])
    y_test = to_categorical(test_labels, num_classes=param["classes"])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Create labels for each key byte hypothesis for test set in order to calculate key ranking
    # -----------------------------------------------------------------------------------------------------------------#
    labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], param["n_test"]))

    for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
        key_h = bytearray.fromhex(param["key"])
        key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
        crypto.set_key(key_h)
        labels_key_hypothesis[key_byte_hypothesis][:] = crypto.create_labels(trace_data_test, aes_leakage_model, param,
                                                                             key_hypothesis=True)

    model_name = ""
    model = None

    for run in range(number_of_runs):
        print("Run: " + str(run))

        # -------------------------------------------------------------------------------------------------------------#
        #  Declare callbacks
        # -------------------------------------------------------------------------------------------------------------#
        callback_key_ranking_train = KeyRankingCallbackTrainingEnd(x_test, labels_key_hypothesis, param)

        # -------------------------------------------------------------------------------------------------------------#
        #  Declare neural network model and run training by calling .fit()
        # -------------------------------------------------------------------------------------------------------------#
        model_obj = DeepLearningModel()
        model_name_obj = model_obj.basic_mlp_ches_ctf
        model = model_name_obj(param["classes"], param["number_of_samples"])
        history = model.fit(x=x_train,
                            y=y_train,
                            batch_size=param["mini-batch"],
                            verbose=1,
                            epochs=param["epochs"],
                            shuffle=True,
                            validation_data=(x_validation, y_validation),
                            callbacks=[callback_key_ranking_train])
        model_name = model_name_obj.__name__
        learning_rate = backend.eval(model.optimizer.lr)
        optimizer = model.optimizer.__class__.__name__

        # ------------------------------------------------------------------------------------------------------------ #
        #  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
        # ------------------------------------------------------------------------------------------------------------ #
        accuracy_training[key_byte_index] += history.history['accuracy']
        accuracy_validation[key_byte_index] += history.history['val_accuracy']
        recall_training[key_byte_index] += history.history['recall']
        recall_validation[key_byte_index] += history.history['val_recall']
        loss_training[key_byte_index] += history.history['loss']
        loss_validation[key_byte_index] += history.history['val_loss']
        key_rank_test_all_keys[key_byte_index][run] = callback_key_ranking_train.get_key_ranking()

        backend.clear_session()

    elapsed_time = time.time() - start

    # ---------------------------------------------------------------------------------------------------------------- #
    #  Compute averaged metrics
    # -----------------------------------------------------------------------------------------------------------------#
    accuracy_training[key_byte_index] /= number_of_runs
    accuracy_validation[key_byte_index] /= number_of_runs
    recall_training[key_byte_index] /= number_of_runs
    recall_validation[key_byte_index] /= number_of_runs
    loss_training[key_byte_index] /= number_of_runs
    loss_validation[key_byte_index] /= number_of_runs

    # ---------------------------------------------------------------------------------------------------------------- #
    #  Compute Guessing Entropy for all key bytes = Averaged Key Ranking over multiple trainings
    # ---------------------------------------------------------------------------------------------------------------- #
    for run in range(number_of_runs):
        guessing_entropy_all_keys[key_byte_index] += key_rank_test_all_keys[key_byte_index][run]

    guessing_entropy_all_keys[key_byte_index] /= number_of_runs

    # ---------------------------------------------------------------------------------------------------------------- #
    #  Compute Success Rates for all key bytes
    # ---------------------------------------------------------------------------------------------------------------- #
    for index in range(number_of_traces_test):
        for run in range(number_of_runs):
            if key_rank_test_all_keys[key_byte_index][run][index] == 1:
                success_rate_all_keys[key_byte_index][index] += 1

    success_rate_all_keys[key_byte_index] /= number_of_runs

    # ---------------------------------------------------------------------------------------------------------------------#
    #  Save results in database
    # ---------------------------------------------------------------------------------------------------------------------#
    if first_key_byte:
        database_inserts = DatabaseInserts('database.sqlite', "script_multiple_all_keys.py", elapsed_time,
                                           multiple_trainings=True)
        first_key_byte = False
    else:
        database_inserts.update_elapsed_time_analysis(elapsed_time)

    database_inserts.save_neural_network("MLP 4 layers", model_name)
    database_inserts.save_training_hyper_parameters(mini_batch=param["mini-batch"],
                                                    epochs=param["epochs"],
                                                    learning_rate=learning_rate,
                                                    optimizer=optimizer,
                                                    training_set=param["n_train"],
                                                    validation_set=param["n_validation"],
                                                    test_set=param["n_test"])
    database_inserts.save_leakage_model(cipher="AES",
                                        leakage_model=aes_leakage_model["leakage_model"],
                                        operation=aes_leakage_model["state_output"])
    database_inserts.save_metric(accuracy_training[key_byte_index], aes_leakage_model["byte"], "accuracy")
    database_inserts.save_metric(accuracy_validation[key_byte_index], aes_leakage_model["byte"], "val_accuracy")
    database_inserts.save_metric(recall_training[key_byte_index], aes_leakage_model["byte"], "recall")
    database_inserts.save_metric(recall_validation[key_byte_index], aes_leakage_model["byte"], "val_recall")
    database_inserts.save_metric(loss_training[key_byte_index], aes_leakage_model["byte"], "loss")
    database_inserts.save_metric(loss_validation[key_byte_index], aes_leakage_model["byte"], "val_loss")
    database_inserts.save_key_rank_json(pd.Series(guessing_entropy_all_keys[key_byte_index]).to_json(),
                                        aes_leakage_model["byte"], "Test Traces")
    database_inserts.save_success_rate_json(pd.Series(success_rate_all_keys[key_byte_index]).to_json(),
                                            aes_leakage_model["byte"], "Test Traces")

# -------------------------------------------------------------------------------------------------------------------- #
#  Plots
# -------------------------------------------------------------------------------------------------------------------- #

plots = Plots(rows=2, cols=2)
plots.new_plot()

values = range(number_of_target_key_bytes)
jet = cm = plots.get_color_map()
cNorm = colors.Normalize(vmin=0, vmax=values[-1])
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

list_of_series = []
for key_byte_index in range(number_of_target_key_bytes):
    colorVal = scalarMap.to_rgba(key_byte_index)
    list_of_series.append(
        {"data": accuracy_training[key_byte_index],
         "label": "key byte " + str(key_byte_index) + " (Training)", "color": colorVal, "legend": True}
    )
for key_byte_index in range(number_of_target_key_bytes):
    colorVal = scalarMap.to_rgba(key_byte_index)
    list_of_series.append(
        {"data": accuracy_validation[key_byte_index],
         "label": "key byte " + str(key_byte_index) + " (Validation)", "color": colorVal, "legend": True}
    )
plots.create_line_plot(1, list_of_series, "Epochs", "Accuracy", show_legend=True, legend_out=True)

list_of_series = []
for key_byte_index in range(number_of_target_key_bytes):
    colorVal = scalarMap.to_rgba(key_byte_index)
    list_of_series.append(
        {"data": recall_training[key_byte_index],
         "label": "key byte " + str(key_byte_index) + " (Training)", "color": colorVal, "legend": True}
    )
for key_byte_index in range(number_of_target_key_bytes):
    colorVal = scalarMap.to_rgba(key_byte_index)
    list_of_series.append(
        {"data": recall_validation[key_byte_index],
         "label": "key byte " + str(key_byte_index) + " (Validation)", "color": colorVal, "legend": True}
    )
plots.create_line_plot(2, list_of_series, "Epochs", "Recall", show_legend=True, legend_out=True)

list_of_series = []
for key_byte_index in range(number_of_target_key_bytes):
    colorVal = scalarMap.to_rgba(key_byte_index)
    list_of_series.append(
        {"data": loss_training[key_byte_index],
         "label": "key byte " + str(key_byte_index) + " (Training)", "color": colorVal, "legend": True}
    )
for key_byte_index in range(number_of_target_key_bytes):
    colorVal = scalarMap.to_rgba(key_byte_index)
    list_of_series.append(
        {"data": loss_validation[key_byte_index],
         "label": "key byte " + str(key_byte_index) + " (Validation)", "color": colorVal, "legend": True}
    )
plots.create_line_plot(3, list_of_series, "Epochs", "Loss", show_legend=True, legend_out=True)

list_of_series = []
for key_byte_index in range(number_of_target_key_bytes):
    colorVal = scalarMap.to_rgba(key_byte_index)
    list_of_series.append(
        {"data": guessing_entropy_all_keys[key_byte_index],
         "label": "key byte " + str(key_byte_index), "color": colorVal, "legend": True}
    )
plots.create_line_plot(4, list_of_series, "Number of Traces", "Guessing Entropy", show_legend=True, legend_out=True)

plots.show_plot()

backend.clear_session()
