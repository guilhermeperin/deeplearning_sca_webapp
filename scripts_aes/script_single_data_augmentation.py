import numpy as np
from keras import backend as backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import TrainingCallback, ValidationCallback, KeyRankingCallbackEpochEnd, \
    KeyRankingCallbackTrainingEnd
from commons.sca_datasets import ScaDataSets
from commons.sca_plots import Plots
import random

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#
trace_directory_path = 'D:/traces/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("ches_ctf")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 0-10
#  "round_state_output": 0-10
#  "state_input": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  "state_output": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW, HD, ID, bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#
aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "ID",
    "bit": 0,
    "byte": 0,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#
data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, x_train_d, x_val_d, _, _ = data_sets.load_training_and_validation_sets()
x_test, trace_data_test, _ = data_sets.load_test_set()

# ---------------------------------------------------------------------------------------------------------------------#
#  Create categorical labels from selected leakage model and cipher
#  A categorical label is a vector with all zeros, except at the index of the class/label
#  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
# ---------------------------------------------------------------------------------------------------------------------#
crypto = AES()
train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
test_labels = crypto.create_labels(trace_data_test, aes_leakage_model, param)

y_train = to_categorical(train_labels, num_classes=param["classes"])
y_validation = to_categorical(validation_labels, num_classes=param["classes"])
y_test = to_categorical(test_labels, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Create labels for each key byte hypothesis for test set in order to calculate key ranking
# ---------------------------------------------------------------------------------------------------------------------#
labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], param["n_test"]))

for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
    key_h = bytearray.fromhex(param["key"])
    key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
    crypto.set_key(key_h)
    labels_key_hypothesis[key_byte_hypothesis][:] = crypto.create_labels(trace_data_test, aes_leakage_model, param,
                                                                         key_hypothesis=True)


# ---------------------------------------------------------------------------------------------------------------------#
#  Functions for Data Augmentation
# ---------------------------------------------------------------------------------------------------------------------#
def data_augmentation_shifts(data_set_samples, data_set_labels):
    ns = param["number_of_samples"]

    while True:

        x_train_shifted = np.zeros((param["mini-batch"], ns))
        rnd = random.randint(0, len(data_set_samples) - param["mini-batch"])
        x_mini_batch = data_set_samples[rnd:rnd + param["mini-batch"]]

        for trace_index in range(param["mini-batch"]):
            x_train_shifted[trace_index] = x_mini_batch[trace_index]
            # shift = random.randint(-5, 5)
            # if shift > 0:
            #     x_train_shifted[trace_index][0:ns - shift] = x_mini_batch[trace_index][shift:ns]
            #     x_train_shifted[trace_index][ns - shift:ns] = x_mini_batch[trace_index][0:shift]
            # else:
            #     x_train_shifted[trace_index][0:abs(shift)] = x_mini_batch[trace_index][ns - abs(shift):ns]
            #     x_train_shifted[trace_index][abs(shift):ns] = x_mini_batch[trace_index][0:ns - abs(shift)]

            noise = np.random.normal(0, 0.01, ns)
            x_train_shifted[trace_index] += noise

        yield x_train_shifted.reshape((x_train_shifted.shape[0], x_train_shifted.shape[1], 1)), data_set_labels[
                                                                                                rnd:rnd + param[
                                                                                                    "mini-batch"]]


def data_augmentation_noise(data_set_samples, data_set_labels):
    ns = param["number_of_samples"]

    while True:
        x_train_modified = np.zeros((len(data_set_samples), ns))

        for trace_index in range(len(data_set_samples)):
            noise = np.random.normal(0, 0.1, ns)
            x_train_modified[trace_index] = data_set_samples[trace_index] + noise

        yield (x_train_modified.reshape((x_train_modified.shape[0], x_train_modified.shape[1], 1)), data_set_labels)


# ---------------------------------------------------------------------------------------------------------------------#
#  Declare callbacks
# ---------------------------------------------------------------------------------------------------------------------#
callback_key_ranking_train = KeyRankingCallbackTrainingEnd(x_test, labels_key_hypothesis, param)

# ---------------------------------------------------------------------------------------------------------------------#
#  Declare neural network model and run training by calling .fit()
# ---------------------------------------------------------------------------------------------------------------------#

model_obj = DeepLearningModel()
model_name_obj = model_obj.basic_mlp_ches_ctf
model = model_name_obj(param["classes"], param["number_of_samples"])

history = model.fit_generator(
    generator=data_augmentation_shifts(x_train_d, y_train),
    steps_per_epoch=1000,
    epochs=param["epochs"],
    validation_data=data_augmentation_shifts(x_val_d, y_validation),
    validation_steps=1,
    callbacks=[callback_key_ranking_train]
)

# ---------------------------------------------------------------------------------------------------------------------#
#  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
# ---------------------------------------------------------------------------------------------------------------------#

accuracy_training = history.history["accuracy"]
accuracy_validation = history.history["val_accuracy"]

recall_training = history.history["recall"]
recall_validation = history.history["val_recall"]

loss_training = history.history["loss"]
loss_validation = history.history["val_loss"]

key_rank = callback_key_ranking_train.get_key_ranking()

# ---------------------------------------------------------------------------------------------------------------------#
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(1, 2)
plots.new_plot()
list_of_series = [
    {
        "data": accuracy_training,
        "label": "Training",
        "color": "blue",
        "legend": True
    },
    {
        "data": accuracy_validation,
        "label": "Validation",
        "color": "orange",
        "legend": True
    }
]
plots.create_line_plot(1, list_of_series, "Epochs", "Accuracy", show_legend=True)
list_of_series = [
    {
        "data": key_rank,
        "label": "Correct Key",
        "color": "blue",
        "legend": True
    }
]
plots.create_line_plot(2, list_of_series, "Number of Traces", "Key Rank", show_legend=True)
plots.show_plot()

backend.clear_session()
