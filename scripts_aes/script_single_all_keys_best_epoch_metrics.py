import numpy as np
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import ValidationCallback, KeyRankingCallbackTrainingEnd, CalculateITY
from commons.sca_datasets import ScaDataSets
from database.database_inserts import DatabaseInserts
from commons.sca_plots import Plots
import matplotlib.colors as colors
import matplotlib.cm as cmx
import pandas as pd
import time

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#
trace_directory_path = 'D:/traces/'
sca_parameters = ScaParameters()
param = sca_parameters.get_trace_set("ches_ctf")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 0-10
#  "round_state_output": 0-10
#  "state_input": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  "state_output": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW, HD, ID, bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#
aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "HW",
    "bit": 0,
    "byte": 0,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#
data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, _, validation_dataset, _, _ = data_sets.load_training_and_validation_sets()
x_test, trace_data_test, _ = data_sets.load_test_set()

# ---------------------------------------------------------------------------------------------------------------------#
#  Obtain test key as an int array
# ---------------------------------------------------------------------------------------------------------------------#
key_int = ([int(x) for x in bytearray.fromhex(param["key"])])

# ---------------------------------------------------------------------------------------------------------------------#
#  Loop for the number of training runs (for success rate calculation)
# ---------------------------------------------------------------------------------------------------------------------#
number_of_target_key_bytes = len(key_int)
number_of_traces_test = param["n_test"]
key_ranks = np.zeros((number_of_target_key_bytes, number_of_traces_test))

best_key_rankings_acc = np.zeros((number_of_target_key_bytes, number_of_traces_test))
best_key_rankings_recall = np.zeros((number_of_target_key_bytes, number_of_traces_test))
best_key_rankings_loss = np.zeros((number_of_target_key_bytes, number_of_traces_test))
best_key_rankings_validation_key_rank = np.zeros((number_of_target_key_bytes, number_of_traces_test))

first_key_byte = True
database_inserts = None
start = time.time()

# ---------------------------------------------------------------------------------------------------------------------#
#  Run train, validation and test for all target key bytes
# ---------------------------------------------------------------------------------------------------------------------#
for key_byte_index in range(number_of_target_key_bytes):

    print("key byte:" + str(key_byte_index))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Update leakage model and target key byte information
    # -----------------------------------------------------------------------------------------------------------------#
    param["good_key"] = key_int[key_byte_index]
    aes_leakage_model["byte"] = key_byte_index

    # ---------------------------------------------------------------------------------------------------------------------#
    #  Create categorical labels from selected leakage model and cipher
    #  A categorical label is a vector with all zeros, except at the index of the class/label
    #  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
    # ---------------------------------------------------------------------------------------------------------------------#
    crypto = AES()
    train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
    validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
    test_labels = crypto.create_labels(trace_data_test, aes_leakage_model, param)

    y_train = to_categorical(train_labels, num_classes=param["classes"])
    y_validation = to_categorical(validation_labels, num_classes=param["classes"])
    y_test = to_categorical(test_labels, num_classes=param["classes"])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Create labels for each key byte hypothesis for test set in order to calculate key ranking
    # -----------------------------------------------------------------------------------------------------------------#
    labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], param["n_test"]))

    for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
        key_h = bytearray.fromhex(param["key"])
        key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
        crypto.set_key(key_h)
        labels_key_hypothesis[key_byte_hypothesis][:] = crypto.create_labels(trace_data_test, aes_leakage_model, param,
                                                                             key_hypothesis=True)

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#
    callbacks_validation = ValidationCallback(x_validation, y_validation, save_model=True)
    callback_key_ranking_train = KeyRankingCallbackTrainingEnd(x_test, labels_key_hypothesis, param)
    callback_mia_validation_ity = CalculateITY(validation_dataset, y_validation, 100, save_model=True)

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    # -----------------------------------------------------------------------------------------------------------------#

    model_obj = DeepLearningModel()
    model_name_obj = model_obj.basic_mlp_ches_ctf
    model = model_name_obj(param["classes"], param["number_of_samples"])
    history = model.fit(x=x_train,
                        y=y_train,
                        batch_size=param["mini-batch"],
                        verbose=1,
                        epochs=param["epochs"],
                        shuffle=True,
                        validation_data=(x_validation, y_validation),
                        callbacks=[callbacks_validation, callback_key_ranking_train, callback_mia_validation_ity])
    elapsed_time = time.time() - start

    # ---------------------------------------------------------------------------------------------------------------- #
    #  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
    # ---------------------------------------------------------------------------------------------------------------- #
    key_ranks[key_byte_index] = callback_key_ranking_train.get_key_ranking()
    ity_validation = callback_mia_validation_ity.get_ity()

    # ---------------------------------------------------------------------------------------------------------------------#
    # Process Key Ranks
    # ---------------------------------------------------------------------------------------------------------------------#
    model_ity = model_name_obj(param["classes"], param["number_of_samples"])
    model_ity.load_weights("best_model_ity.h5")
    model_acc = model_name_obj(param["classes"], param["number_of_samples"])
    model_acc.load_weights("best_model_accuracy.h5")
    model_loss = model_name_obj(param["classes"], param["number_of_samples"])
    model_loss.load_weights("best_model_loss.h5")
    model_recall = model_name_obj(param["classes"], param["number_of_samples"])
    model_recall.load_weights("best_model_recall.h5")

    output_probabilities_ity = model_ity.predict(x_test)
    output_probabilities_acc = model_acc.predict(x_test)
    output_probabilities_loss = model_loss.predict(x_test)
    output_probabilities_recall = model_recall.predict(x_test)

    key_probabilities_ity = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_acc = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_loss = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_recall = np.zeros(param["number_of_key_hypothesis"])

    key_ranking_ity = np.zeros(param["n_test"])
    key_ranking_acc = np.zeros(param["n_test"])
    key_ranking_loss = np.zeros(param["n_test"])
    key_ranking_recall = np.zeros(param["n_test"])

    for index in range(param["n_test"]):

        probabilities_kg_ity = output_probabilities_ity[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_ity += np.log(probabilities_kg_ity)

        probabilities_kg_acc = output_probabilities_acc[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_acc += np.log(probabilities_kg_acc)

        probabilities_kg_loss = output_probabilities_loss[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_loss += np.log(probabilities_kg_loss)

        probabilities_kg_recall = output_probabilities_recall[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_recall += np.log(probabilities_kg_recall)

        key_probabilities_sorted_ity = np.argsort(key_probabilities_ity)[::-1]
        key_probabilities_sorted_acc = np.argsort(key_probabilities_acc)[::-1]
        key_probabilities_sorted_loss = np.argsort(key_probabilities_loss)[::-1]
        key_probabilities_sorted_recall = np.argsort(key_probabilities_recall)[::-1]

        for kg in range(param["number_of_key_hypothesis"]):

            if key_probabilities_sorted_ity[kg] == param["good_key"]:
                key_ranking_ity[index] = kg + 1

            if key_probabilities_sorted_acc[kg] == param["good_key"]:
                key_ranking_acc[index] = kg + 1

            if key_probabilities_sorted_loss[kg] == param["good_key"]:
                key_ranking_loss[index] = kg + 1

            if key_probabilities_sorted_recall[kg] == param["good_key"]:
                key_ranking_recall[index] = kg + 1

    # ---------------------------------------------------------------------------------------------------------------------#
    #  Save results in database
    # ---------------------------------------------------------------------------------------------------------------------#
    if first_key_byte:
        database_inserts = DatabaseInserts('database.sqlite', "script_single_all_keys_best_epoch_metrics.py",
                                           elapsed_time)
        first_key_byte = False
    else:
        database_inserts.update_elapsed_time_analysis(elapsed_time)

    database_inserts.save_neural_network("MLP 4 layers", model_name_obj.__name__)
    database_inserts.save_training_hyper_parameters(mini_batch=param["mini-batch"],
                                                    epochs=param["epochs"],
                                                    learning_rate=backend.eval(model.optimizer.lr),
                                                    optimizer=model.optimizer.__class__.__name__,
                                                    training_set=param["n_train"],
                                                    validation_set=param["n_validation"],
                                                    test_set=param["n_test"])
    database_inserts.save_leakage_model(cipher="AES",
                                        leakage_model=aes_leakage_model["leakage_model"],
                                        operation=aes_leakage_model["state_output"])
    for metric in sca_parameters.get_basic_metrics():
        database_inserts.save_metric(history.history[metric], aes_leakage_model["byte"], metric)
    database_inserts.save_metric(ity_validation, aes_leakage_model["byte"], "I(T,Y)")
    database_inserts.save_key_rank_json(pd.Series(key_ranks[key_byte_index]).to_json(), aes_leakage_model["byte"],
                                        "All epochs")
    database_inserts.save_key_rank_json(pd.Series(key_ranking_acc).to_json(), aes_leakage_model["byte"],
                                        "Val. Accuracy")
    database_inserts.save_key_rank_json(pd.Series(key_ranking_recall).to_json(), aes_leakage_model["byte"],
                                        "Val. Recall")
    database_inserts.save_key_rank_json(pd.Series(key_ranking_loss).to_json(), aes_leakage_model["byte"], "Val. Loss")
    database_inserts.save_key_rank_json(pd.Series(key_ranking_ity).to_json(), aes_leakage_model["byte"], "I(T,Y)")

    backend.clear_session()

# -------------------------------------------------------------------------------------------------------------------- #
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(rows=1, cols=1)
plots.new_plot()

values = range(number_of_target_key_bytes)
jet = cm = plots.get_color_map()
cNorm = colors.Normalize(vmin=0, vmax=values[-1])
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

list_of_series = []
for key_byte_index in range(number_of_target_key_bytes):
    colorVal = scalarMap.to_rgba(key_byte_index)
    list_of_series.append(
        {"data": key_ranks[key_byte_index],
         "label": "key byte " + str(key_byte_index), "color": colorVal, "legend": True}
    )
plots.create_line_plot(1, list_of_series, "Number of Traces", "Key Ranking", show_legend=True, legend_out=True)

plots.show_plot()

backend.clear_session()
