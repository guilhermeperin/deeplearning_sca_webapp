import numpy as np
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import KeyRankingCallbackTrainingEnd, CalculateITY
from commons.sca_datasets import ScaDataSets
from database.database_inserts import DatabaseInserts
import random
import time

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'D:/traces/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("ches_ctf")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 0-10
#  "round_state_output": 0-10
#  "state_input": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  "state_output": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW, HD, ID, bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#

aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "HW",
    "bit": 0,
    "byte": 0,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#
data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, _, validation_dataset, _, _ = data_sets.load_training_and_validation_sets()
x_test, trace_data_test, _ = data_sets.load_test_set()

# ---------------------------------------------------------------------------------------------------------------------#
#  Create categorical labels from selected leakage model and cipher
#  A categorical label is a vector with all zeros, except at the index of the class/label
#  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
# ---------------------------------------------------------------------------------------------------------------------#
crypto = AES()
train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
test_labels = crypto.create_labels(trace_data_test, aes_leakage_model, param)

y_train = to_categorical(train_labels, num_classes=param["classes"])
y_validation = to_categorical(validation_labels, num_classes=param["classes"])
y_test = to_categorical(test_labels, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Create labels for each key byte hypothesis for test set in order to calculate key ranking
# ---------------------------------------------------------------------------------------------------------------------#
labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], param["n_test"]))

for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
    key_h = bytearray.fromhex(param["key"])
    key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
    crypto.set_key(key_h)
    labels_key_hypothesis[key_byte_hypothesis][:] = crypto.create_labels(trace_data_test, aes_leakage_model, param,
                                                                         key_hypothesis=True)

# ---------------------------------------------------------------------------------------------------------------------#
#  Loop for the number of training runs (for success rate calculation)
# ---------------------------------------------------------------------------------------------------------------------#

hyper_parameters = []
number_of_runs = 20
model = None
start = time.time()

for run in range(number_of_runs):
    print("Run: " + str(run))
    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#
    callback_key_ranking_train = KeyRankingCallbackTrainingEnd(x_test, labels_key_hypothesis, param)
    callback_mia_validation_ity = CalculateITY(validation_dataset, y_validation, 100, save_model=True)
    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    # -----------------------------------------------------------------------------------------------------------------#
    mini_batch = random.randrange(100, 200, 100)
    # epochs = random.randrange(5, 10, 5)
    epochs = 20
    activation = random.choice(['relu', 'selu', 'elu'])
    neurons = random.randrange(20, 200, 10)
    layers = random.randrange(1, 4, 1)
    learning_rate = random.uniform(0.0001, 0.01)

    model_obj = DeepLearningModel()
    model_name_obj = model_obj.basic_mlp_random
    model = model_name_obj(classes=param["classes"],
                           number_of_samples=param["number_of_samples"],
                           activation=activation,
                           neurons=neurons,
                           layers=layers,
                           learning_rate=learning_rate)
    history = model.fit(x=x_train,
                        y=y_train,
                        batch_size=mini_batch,
                        verbose=1,
                        epochs=epochs,
                        shuffle=True,
                        validation_data=(x_validation, y_validation),
                        callbacks=[callback_key_ranking_train, callback_mia_validation_ity])

    # ---------------------------------------------------------------------------------------------------------------- #
    #  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------------#
    #  Retrieve metrics from callbacks
    # ---------------------------------------------------------------------------------------------------------------------#
    ity_validation = callback_mia_validation_ity.get_ity()
    key_rank_final_epoch = callback_key_ranking_train.get_key_ranking()

    # ---------------------------------------------------------------------------------------------------------------------#
    # Process Key Rank
    # ---------------------------------------------------------------------------------------------------------------------#
    model_ity = model_name_obj(classes=param["classes"],
                               number_of_samples=param["number_of_samples"],
                               activation=activation,
                               neurons=neurons,
                               layers=layers,
                               learning_rate=learning_rate)
    model_ity.load_weights("best_model_ity.h5")

    output_probabilities_ity = model_ity.predict(x_test)
    key_probabilities_ity = np.zeros(param["number_of_key_hypothesis"])
    key_ranking_ity = np.zeros(param["n_test"])

    for index in range(param["n_test"]):

        probabilities_kg_ity = output_probabilities_ity[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_ity += np.log(probabilities_kg_ity)

        key_probabilities_sorted_ity = np.argsort(key_probabilities_ity)[::-1]

        for kg in range(param["number_of_key_hypothesis"]):

            if key_probabilities_sorted_ity[kg] == param["good_key"]:
                key_ranking_ity[index] = kg + 1

    hyper_parameters.append({
        "key_rank": key_ranking_ity[len(key_ranking_ity) - 1],
        "key_rank_all_epochs": key_rank_final_epoch[len(key_rank_final_epoch) - 1],
        "max_ity": max(ity_validation),
        "mini_batch": mini_batch,
        "epochs": epochs,
        "activation": activation,
        "neurons": neurons,
        "layers": layers,
        "learning_rate": learning_rate
    })

    backend.clear_session()

elapsed_time = time.time() - start

# ---------------------------------------------------------------------------------------------------------------------#
#  Save results in database
# ---------------------------------------------------------------------------------------------------------------------#
database_inserts = DatabaseInserts('database.sqlite', "script_multiple_hyper_parameters_best_epochs_metrics.py",
                                   elapsed_time, model_fixed=False, multiple_trainings=True)
database_inserts.save_hyper_parameters(hyper_parameters)

backend.clear_session()
