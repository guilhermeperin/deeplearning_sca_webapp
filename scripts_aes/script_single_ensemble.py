import numpy as np
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import KeyProbabilitiesCallbackTrainingEnd, KeyRankingCallbackTrainingEnd
from commons.sca_datasets import ScaDataSets
from commons.sca_plots import Plots
from database.database_inserts import DatabaseInserts
import matplotlib.colors as colors
import matplotlib.cm as cmx
import pandas as pd
import time

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#
trace_directory_path = 'D:/traces/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("ches_ctf")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 0-10
#  "round_state_output": 0-10
#  "state_input": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  "state_output": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW, HD, ID, bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#

aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "HW",
    "bit": 0,
    "byte": 0,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#

data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, train_dataset, validation_dataset, _, _ = data_sets.load_training_and_validation_sets(
    random_order=True)
x_test_fixed_key, x_validation_fixed_key, trace_data_test_fixed_key, trace_data_validation_fixed_key = data_sets.load_two_tests()

# ---------------------------------------------------------------------------------------------------------------------#
#  Create categorical labels from selected leakage model and cipher
#  A categorical label is a vector with all zeros, except at the index of the class/label
#  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
# ---------------------------------------------------------------------------------------------------------------------#

crypto = AES()
train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
validation_labels_fixed_key = crypto.create_labels(trace_data_validation_fixed_key, aes_leakage_model, param)
test_labels_fixed_key = crypto.create_labels(trace_data_test_fixed_key, aes_leakage_model, param)

y_train = to_categorical(train_labels, num_classes=param["classes"])
y_validation = to_categorical(validation_labels, num_classes=param["classes"])
y_validation_fixed_key = to_categorical(validation_labels_fixed_key, num_classes=param["classes"])
y_test_fixed_key = to_categorical(test_labels_fixed_key, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Create labels for each key byte hypothesis for test set in order to calculate key ranking
# ---------------------------------------------------------------------------------------------------------------------#

number_of_traces_test = int(param["n_test"] / 2)
labels_key_hypothesis_test_fixed_key = np.zeros((param["number_of_key_hypothesis"], number_of_traces_test))
labels_key_hypothesis_validation_fixed_key = np.zeros((param["number_of_key_hypothesis"], number_of_traces_test))

for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
    key_h = bytearray.fromhex(param["key"])
    key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
    crypto.set_key(key_h)
    labels_key_hypothesis_test_fixed_key[key_byte_hypothesis][:] = crypto.create_labels(
        trace_data_test_fixed_key, aes_leakage_model, param, key_hypothesis=True
    )
    labels_key_hypothesis_validation_fixed_key[key_byte_hypothesis][:] = crypto.create_labels(
        trace_data_validation_fixed_key, aes_leakage_model, param, key_hypothesis=True
    )

# ---------------------------------------------------------------------------------------------------------------------#
#  Loop for the number of training runs (for success rate calculation)
# ---------------------------------------------------------------------------------------------------------------------#

number_of_models = 10
number_of_best_models = 3

key_probabilities_test = np.zeros((number_of_models, param["number_of_key_hypothesis"], number_of_traces_test))
key_probabilities_validation = np.zeros((number_of_models, param["number_of_key_hypothesis"], number_of_traces_test))

key_rank_models_test = np.zeros((number_of_models, number_of_traces_test))
key_rank_models_validation = np.zeros((number_of_models, number_of_traces_test))

accuracy_training = np.zeros((number_of_models, param["epochs"]))
accuracy_validation = np.zeros((number_of_models, param["epochs"]))
recall_training = np.zeros((number_of_models, param["epochs"]))
recall_validation = np.zeros((number_of_models, param["epochs"]))
loss_training = np.zeros((number_of_models, param["epochs"]))
loss_validation = np.zeros((number_of_models, param["epochs"]))

model_name = ""
model = None
learning_rate = 0
optimizer = ""
start = time.time()

for model_index in range(number_of_models):
    print("Model: " + str(model_index))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#
    callback_key_ranking_test = KeyRankingCallbackTrainingEnd(x_test_fixed_key,
                                                              labels_key_hypothesis_test_fixed_key,
                                                              param)
    callback_key_ranking_validation = KeyRankingCallbackTrainingEnd(x_validation_fixed_key,
                                                                    labels_key_hypothesis_validation_fixed_key,
                                                                    param)
    callback_key_probabilities_test = KeyProbabilitiesCallbackTrainingEnd(x_test_fixed_key,
                                                                          labels_key_hypothesis_test_fixed_key,
                                                                          param)

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    # -----------------------------------------------------------------------------------------------------------------#
    model_obj = DeepLearningModel()
    model_name_obj = model_obj.basic_mlp_ches_ctf
    model = model_name_obj(param["classes"], param["number_of_samples"])
    history = model.fit(x=x_train,
                        y=y_train,
                        batch_size=param["mini-batch"],
                        verbose=1,
                        epochs=param["epochs"],
                        shuffle=True,
                        validation_data=(x_validation, y_validation),
                        callbacks=[callback_key_probabilities_test, callback_key_ranking_test,
                                   callback_key_ranking_validation])
    model_name = model_name_obj.__name__
    learning_rate = backend.eval(model.optimizer.lr)
    optimizer = model.optimizer.__class__.__name__

    # ---------------------------------------------------------------------------------------------------------------- #
    # Get metrics
    # ---------------------------------------------------------------------------------------------------------------- #
    accuracy_training[model_index] = history.history['accuracy']
    accuracy_validation[model_index] = history.history['val_accuracy']
    recall_training[model_index] = history.history['recall']
    recall_validation[model_index] = history.history['val_recall']
    loss_training[model_index] = history.history['loss']
    loss_validation[model_index] = history.history['val_loss']

    # ---------------------------------------------------------------------------------------------------------------- #
    # Get Key Probabilities from Callbacks
    # ---------------------------------------------------------------------------------------------------------------- #
    key_probabilities_test[model_index] = callback_key_probabilities_test.get_key_probabilities()
    key_rank_models_test[model_index] = callback_key_ranking_test.get_key_ranking()
    key_rank_models_validation[model_index] = callback_key_ranking_validation.get_key_ranking()

    backend.clear_session()

elapsed_time = time.time() - start

# -------------------------------------------------------------------------------------------------------------------- #
# Sort models by validation key rank
# -------------------------------------------------------------------------------------------------------------------- #
key_ranks_number_of_traces_val = []
for model_index in range(number_of_models):
    if key_rank_models_validation[model_index][number_of_traces_test - 1] == 1:
        for index in range(number_of_traces_test - 1, -1, -1):
            if key_rank_models_validation[model_index][index] != 1:
                key_ranks_number_of_traces_val.append(
                    [key_rank_models_validation[model_index][number_of_traces_test - 1], index + 1, model_index])
                break
    else:
        key_ranks_number_of_traces_val.append(
            [key_rank_models_validation[model_index][number_of_traces_test - 1], number_of_traces_test, model_index])

sorted_models = sorted(key_ranks_number_of_traces_val, key=lambda l: l[:])

best_models_validation = []
for model_index in range(number_of_best_models):
    best_models_validation.append(sorted_models[model_index][2])

key_rank_best_model_based_on_val_key_rank = key_rank_models_test[sorted_models[0][2]]

# -------------------------------------------------------------------------------------------------------------------- #
# Compute average metrics for ensembles
# -------------------------------------------------------------------------------------------------------------------- #
accuracy_training_ensemble = np.zeros(param["epochs"])
accuracy_validation_ensemble = np.zeros(param["epochs"])
recall_training_ensemble = np.zeros(param["epochs"])
recall_validation_ensemble = np.zeros(param["epochs"])
loss_training_ensemble = np.zeros(param["epochs"])
loss_validation_ensemble = np.zeros(param["epochs"])

for model_index in range(number_of_models):
    accuracy_training_ensemble += accuracy_training[model_index]
    accuracy_validation_ensemble += accuracy_validation[model_index]
    recall_training_ensemble += recall_training[model_index]
    recall_validation_ensemble += recall_validation[model_index]
    loss_training_ensemble += loss_training[model_index]
    loss_validation_ensemble += loss_validation[model_index]

accuracy_training_ensemble /= number_of_models
accuracy_validation_ensemble /= number_of_models
recall_training_ensemble /= number_of_models
recall_validation_ensemble /= number_of_models
loss_training_ensemble /= number_of_models
loss_validation_ensemble /= number_of_models

accuracy_training_ensemble_best_models = np.zeros(param["epochs"])
accuracy_validation_ensemble_best_models = np.zeros(param["epochs"])
recall_training_ensemble_best_models = np.zeros(param["epochs"])
recall_validation_ensemble_best_models = np.zeros(param["epochs"])
loss_training_ensemble_best_models = np.zeros(param["epochs"])
loss_validation_ensemble_best_models = np.zeros(param["epochs"])

for model_index in best_models_validation:
    accuracy_training_ensemble_best_models += accuracy_training[model_index]
    accuracy_validation_ensemble_best_models += accuracy_validation[model_index]
    recall_training_ensemble_best_models += recall_training[model_index]
    recall_validation_ensemble_best_models += recall_validation[model_index]
    loss_training_ensemble_best_models += loss_training[model_index]
    loss_validation_ensemble_best_models += loss_validation[model_index]

accuracy_training_ensemble_best_models /= number_of_best_models
accuracy_validation_ensemble_best_models /= number_of_best_models
recall_training_ensemble_best_models /= number_of_best_models
recall_validation_ensemble_best_models /= number_of_best_models
loss_training_ensemble_best_models /= number_of_best_models
loss_validation_ensemble_best_models /= number_of_best_models

# -------------------------------------------------------------------------------------------------------------------- #
# Compute ensembles
# -------------------------------------------------------------------------------------------------------------------- #
key_probabilities_ensemble = np.zeros((param["number_of_key_hypothesis"]))
key_probabilities_ensemble_best_models = np.zeros((param["number_of_key_hypothesis"]))
key_rank_ensemble = np.zeros(number_of_traces_test)
key_rank_ensemble_best_models = np.zeros(number_of_traces_test)

for index in range(number_of_traces_test):
    for kg in range(param["number_of_key_hypothesis"]):
        for model_index in range(number_of_models):
            key_probabilities_ensemble[kg] += key_probabilities_test[model_index][kg][index]
        for model_index in best_models_validation:
            key_probabilities_ensemble_best_models[kg] += key_probabilities_test[model_index][kg][index]

    key_probabilities_ensemble_sorted = np.argsort(key_probabilities_ensemble)[::-1]
    key_probabilities_ensemble_best_models_sorted = np.argsort(key_probabilities_ensemble_best_models)[::-1]

    for kg in range(param["number_of_key_hypothesis"]):
        if key_probabilities_ensemble_sorted[kg] == param["good_key"]:
            key_rank_ensemble[index] = kg + 1
        if key_probabilities_ensemble_best_models_sorted[kg] == param["good_key"]:
            key_rank_ensemble_best_models[index] = kg + 1

# ---------------------------------------------------------------------------------------------------------------------#
#  Save results in database
# ---------------------------------------------------------------------------------------------------------------------#
database_inserts = DatabaseInserts('database.sqlite', "script_single_ensemble.py", elapsed_time)
database_inserts.save_neural_network("MLP 4 layers, 1000 units per layer, Adam(lr=0.001)", model_name)
database_inserts.save_training_hyper_parameters(mini_batch=param["mini-batch"],
                                                epochs=param["epochs"],
                                                learning_rate=learning_rate,
                                                optimizer=optimizer,
                                                training_set=param["n_train"],
                                                validation_set=param["n_validation"],
                                                test_set=param["n_test"])
database_inserts.save_leakage_model(cipher="AES",
                                    leakage_model=aes_leakage_model["leakage_model"],
                                    operation=aes_leakage_model["state_output"])
database_inserts.save_metric(accuracy_training_ensemble, aes_leakage_model["byte"], "accuracy_ensembles")
database_inserts.save_metric(accuracy_validation_ensemble, aes_leakage_model["byte"], "val_accuracy_ensembles")
database_inserts.save_metric(recall_training_ensemble, aes_leakage_model["byte"], "recall_ensembles")
database_inserts.save_metric(recall_validation_ensemble, aes_leakage_model["byte"], "val_recall_ensembles")
database_inserts.save_metric(loss_training_ensemble, aes_leakage_model["byte"], "loss_ensembles")
database_inserts.save_metric(loss_validation_ensemble, aes_leakage_model["byte"], "val_loss_ensembles")
database_inserts.save_metric(accuracy_training_ensemble_best_models, aes_leakage_model["byte"],
                             "accuracy_ensembles_best_models")
database_inserts.save_metric(accuracy_validation_ensemble_best_models, aes_leakage_model["byte"],
                             "val_accuracy_ensembles_best_models")
database_inserts.save_metric(recall_training_ensemble_best_models, aes_leakage_model["byte"],
                             "recall_ensembles_best_models")
database_inserts.save_metric(recall_validation_ensemble_best_models, aes_leakage_model["byte"],
                             "val_recall_ensembles_best_models")
database_inserts.save_metric(loss_training_ensemble_best_models, aes_leakage_model["byte"],
                             "loss_ensembles_best_models")
database_inserts.save_metric(loss_validation_ensemble_best_models, aes_leakage_model["byte"],
                             "val_loss_ensembles_best_models", )
database_inserts.save_key_rank_json(pd.Series(key_rank_ensemble).to_json(), aes_leakage_model["byte"],
                                    "Ensemble " + str(number_of_models) + " Models")
database_inserts.save_key_rank_json(pd.Series(key_rank_ensemble_best_models).to_json(),
                                    aes_leakage_model["byte"],
                                    "Ensemble " + str(number_of_best_models) + " Best Models")
database_inserts.save_key_rank_json(pd.Series(key_rank_best_model_based_on_val_key_rank).to_json(),
                                    aes_leakage_model["byte"], "Best Model (Val. KR)")

# -------------------------------------------------------------------------------------------------------------------- #
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(rows=1, cols=1)

values = range(number_of_models)
jet = cm = plots.get_color_map()
cNorm = colors.Normalize(vmin=0, vmax=values[-1])
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

plots.new_plot()

list_of_series = []
for model_index in range(number_of_models):
    colorVal = scalarMap.to_rgba(model_index)
    list_of_series.append(
        {"data": key_rank_models_test[model_index], "label": "Model " + str(model_index), "color": "lightgrey",
         "legend": True})
list_of_series.append(
    {"data": key_rank_ensemble, "label": "Ensemble", "color": "black", "legend": True}
)
list_of_series.append(
    {"data": key_rank_ensemble_best_models, "label": "Ensemble Best Models", "color": "red", "legend": True}
)
list_of_series.append(
    {"data": key_rank_best_model_based_on_val_key_rank, "label": "Ensemble Best Model (Val. KR)", "color": "green",
     "legend": True}
)
plots.create_line_plot(1, list_of_series, "Number of Traces", "Key Rank", show_legend=True)

plots.show_plot()

backend.clear_session()
