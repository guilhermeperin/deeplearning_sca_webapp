import numpy as np
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import ValidationCallback, KeyRankingCallbackTrainingEnd, CalculateITY
from commons.sca_datasets import ScaDataSets
from database.database_inserts import DatabaseInserts
from commons.sca_plots import Plots
import pandas as pd
import time

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#
trace_directory_path = 'D:/traces/'
sca_parameters = ScaParameters()
param = sca_parameters.get_trace_set("ascad_random_key")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 0-10
#  "round_state_output": 0-10
#  "state_input": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  "state_output": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW, HD, ID, bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#
aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "HW",
    "bit": 0,
    "byte": 2,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#
data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, _, validation_dataset, _, _ = data_sets.load_training_and_validation_sets()
x_test, trace_data_test, _ = data_sets.load_test_set()

# ---------------------------------------------------------------------------------------------------------------------#
#  Create categorical labels from selected leakage model and cipher
#  A categorical label is a vector with all zeros, except at the index of the class/label
#  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
# ---------------------------------------------------------------------------------------------------------------------#
crypto = AES()
train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
test_labels = crypto.create_labels(trace_data_test, aes_leakage_model, param)

y_train = to_categorical(train_labels, num_classes=param["classes"])
y_validation = to_categorical(validation_labels, num_classes=param["classes"])
y_test = to_categorical(test_labels, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Create labels for each key byte hypothesis for test set in order to calculate key ranking
# ---------------------------------------------------------------------------------------------------------------------#
labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], param["n_test"]))

for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
    key_h = bytearray.fromhex(param["key"])
    key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
    crypto.set_key(key_h)
    labels_key_hypothesis[key_byte_hypothesis][:] = crypto.create_labels(trace_data_test, aes_leakage_model, param,
                                                                         key_hypothesis=True)

# ---------------------------------------------------------------------------------------------------------------------#
#  Loop for the number of training runs (for success rate calculation)
# ---------------------------------------------------------------------------------------------------------------------#
number_of_runs = 10

accuracy_training = np.zeros(param["epochs"])
accuracy_validation = np.zeros(param["epochs"])
recall_training = np.zeros(param["epochs"])
recall_validation = np.zeros(param["epochs"])
loss_training = np.zeros(param["epochs"])
loss_validation = np.zeros(param["epochs"])
ity_validation = np.zeros(param["epochs"])
key_rank_all_runs = np.zeros((number_of_runs, param["n_test"]))

key_ranking_ity = np.zeros((number_of_runs, param["n_test"]))
key_ranking_acc = np.zeros((number_of_runs, param["n_test"]))
key_ranking_loss = np.zeros((number_of_runs, param["n_test"]))
key_ranking_recall = np.zeros((number_of_runs, param["n_test"]))

model = None
model_name = ""
learning_rate = 0
optimizer = ""
start = time.time()

for run in range(number_of_runs):
    print("Run: " + str(run))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#
    callbacks_validation = ValidationCallback(x_validation, y_validation, save_model=True)
    callback_key_ranking_train = KeyRankingCallbackTrainingEnd(x_test, labels_key_hypothesis, param)
    callback_mia_validation_ity = CalculateITY(validation_dataset, y_validation, 100, save_model=True)

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    # -----------------------------------------------------------------------------------------------------------------#

    model_obj = DeepLearningModel()
    model_name_obj = model_obj.basic_mlp_ches_ctf
    model = model_name_obj(param["classes"], param["number_of_samples"])
    history = model.fit(x=x_train,
                        y=y_train,
                        batch_size=param["mini-batch"],
                        verbose=1,
                        epochs=param["epochs"],
                        shuffle=True,
                        validation_data=(x_validation, y_validation),
                        callbacks=[callbacks_validation, callback_key_ranking_train, callback_mia_validation_ity])
    model_name = model_name_obj.__name__
    learning_rate = backend.eval(model.optimizer.lr)
    optimizer = model.optimizer.__class__.__name__

    # ---------------------------------------------------------------------------------------------------------------- #
    #  Retrieve metrics from callbacks
    # ---------------------------------------------------------------------------------------------------------------- #
    accuracy_training += history.history['accuracy']
    accuracy_validation += history.history['val_accuracy']
    recall_training += history.history['recall']
    recall_validation += history.history['val_recall']
    loss_training += history.history['loss']
    loss_validation += history.history['val_loss']
    ity_validation += callback_mia_validation_ity.get_ity()
    key_rank_all_runs[run] = callback_key_ranking_train.get_key_ranking()

    # ---------------------------------------------------------------------------------------------------------------------#
    # Process Key Ranks
    # ---------------------------------------------------------------------------------------------------------------------#
    model_ity = model_name_obj(param["classes"], param["number_of_samples"])
    model_ity.load_weights("best_model_ity.h5")
    model_acc = model_name_obj(param["classes"], param["number_of_samples"])
    model_acc.load_weights("best_model_accuracy.h5")
    model_loss = model_name_obj(param["classes"], param["number_of_samples"])
    model_loss.load_weights("best_model_loss.h5")
    model_recall = model_name_obj(param["classes"], param["number_of_samples"])
    model_recall.load_weights("best_model_recall.h5")

    output_probabilities_ity = model_ity.predict(x_test)
    output_probabilities_acc = model_acc.predict(x_test)
    output_probabilities_loss = model_loss.predict(x_test)
    output_probabilities_recall = model_recall.predict(x_test)

    key_probabilities_ity = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_acc = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_loss = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_recall = np.zeros(param["number_of_key_hypothesis"])

    for index in range(param["n_test"]):

        probabilities_kg_ity = output_probabilities_ity[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_ity += np.log(probabilities_kg_ity)

        probabilities_kg_acc = output_probabilities_acc[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_acc += np.log(probabilities_kg_acc)

        probabilities_kg_loss = output_probabilities_loss[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_loss += np.log(probabilities_kg_loss)

        probabilities_kg_recall = output_probabilities_recall[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis[:]])]
        key_probabilities_recall += np.log(probabilities_kg_recall)

        key_probabilities_sorted_ity = np.argsort(key_probabilities_ity)[::-1]
        key_probabilities_sorted_acc = np.argsort(key_probabilities_acc)[::-1]
        key_probabilities_sorted_loss = np.argsort(key_probabilities_loss)[::-1]
        key_probabilities_sorted_recall = np.argsort(key_probabilities_recall)[::-1]

        for kg in range(param["number_of_key_hypothesis"]):

            if key_probabilities_sorted_ity[kg] == param["good_key"]:
                key_ranking_ity[run][index] = kg + 1

            if key_probabilities_sorted_acc[kg] == param["good_key"]:
                key_ranking_acc[run][index] = kg + 1

            if key_probabilities_sorted_loss[kg] == param["good_key"]:
                key_ranking_loss[run][index] = kg + 1

            if key_probabilities_sorted_recall[kg] == param["good_key"]:
                key_ranking_recall[run][index] = kg + 1

    backend.clear_session()

elapsed_time = time.time() - start

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute averaged metrics
# ---------------------------------------------------------------------------------------------------------------------#
accuracy_training /= number_of_runs
accuracy_validation /= number_of_runs
recall_training /= number_of_runs
recall_validation /= number_of_runs
loss_training /= number_of_runs
loss_validation /= number_of_runs
ity_validation /= number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute Guessing Entropy = Averaged Key Ranking over multiple trainings
# ---------------------------------------------------------------------------------------------------------------------#

guessing_entropy_all_epochs = np.zeros(param["n_test"])
guessing_entropy_acc = np.zeros(param["n_test"])
guessing_entropy_recall = np.zeros(param["n_test"])
guessing_entropy_loss = np.zeros(param["n_test"])
guessing_entropy_ity = np.zeros(param["n_test"])

for run in range(number_of_runs):
    guessing_entropy_all_epochs += key_rank_all_runs[run]
    guessing_entropy_acc += key_ranking_acc[run]
    guessing_entropy_recall += key_ranking_recall[run]
    guessing_entropy_loss += key_ranking_loss[run]
    guessing_entropy_ity += key_ranking_ity[run]

guessing_entropy_all_epochs /= number_of_runs
guessing_entropy_acc /= number_of_runs
guessing_entropy_recall /= number_of_runs
guessing_entropy_loss /= number_of_runs
guessing_entropy_ity /= number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute Success Rates
# ---------------------------------------------------------------------------------------------------------------------#

success_rate_all_epochs = np.zeros(param["n_test"])
success_rate_acc = np.zeros(param["n_test"])
success_rate_recall = np.zeros(param["n_test"])
success_rate_loss = np.zeros(param["n_test"])
success_rate_ity = np.zeros(param["n_test"])

for index in range(param["n_test"]):
    for run in range(number_of_runs):
        if key_rank_all_runs[run][index] == 1:
            success_rate_all_epochs[index] += 1
        if key_ranking_acc[run][index] == 1:
            success_rate_acc[index] += 1
        if key_ranking_recall[run][index] == 1:
            success_rate_recall[index] += 1
        if key_ranking_loss[run][index] == 1:
            success_rate_loss[index] += 1
        if key_ranking_ity[run][index] == 1:
            success_rate_ity[index] += 1

success_rate_all_epochs /= number_of_runs
success_rate_acc /= number_of_runs
success_rate_recall /= number_of_runs
success_rate_loss /= number_of_runs
success_rate_ity /= number_of_runs

# ---------------------------------------------------------------------------------------------------------------------#
#  Save results in database
# ---------------------------------------------------------------------------------------------------------------------#
database_inserts = DatabaseInserts('database.sqlite', "script_multiple_best_epoch_metrics.py", elapsed_time,
                                   multiple_trainings=True)
database_inserts.save_neural_network("MLP 2 layers, 100 units per layer, Adam(lr=0.001)", model_name)
database_inserts.save_training_hyper_parameters(mini_batch=param["mini-batch"],
                                                epochs=param["epochs"],
                                                learning_rate=learning_rate,
                                                optimizer=optimizer,
                                                training_set=param["n_train"],
                                                validation_set=param["n_validation"],
                                                test_set=param["n_test"])
database_inserts.save_leakage_model(cipher="AES",
                                    leakage_model=aes_leakage_model["leakage_model"],
                                    operation=aes_leakage_model["state_output"])
database_inserts.save_metric(accuracy_training, aes_leakage_model["byte"], "accuracy")
database_inserts.save_metric(accuracy_validation, aes_leakage_model["byte"], "val_accuracy")
database_inserts.save_metric(recall_training, aes_leakage_model["byte"], "recall")
database_inserts.save_metric(recall_validation, aes_leakage_model["byte"], "val_recall")
database_inserts.save_metric(loss_training, aes_leakage_model["byte"], "loss")
database_inserts.save_metric(loss_validation, aes_leakage_model["byte"], "val_loss")
database_inserts.save_metric(ity_validation, aes_leakage_model["byte"], "I(T,Y)")
database_inserts.save_key_rank_json(pd.Series(guessing_entropy_all_epochs).to_json(), aes_leakage_model["byte"],
                                    "All epochs")
database_inserts.save_key_rank_json(pd.Series(guessing_entropy_acc).to_json(), aes_leakage_model["byte"],
                                    "Val. Accuracy")
database_inserts.save_key_rank_json(pd.Series(guessing_entropy_recall).to_json(), aes_leakage_model["byte"],
                                    "Val. Recall")
database_inserts.save_key_rank_json(pd.Series(guessing_entropy_loss).to_json(), aes_leakage_model["byte"],
                                    "Val. Loss")
database_inserts.save_key_rank_json(pd.Series(guessing_entropy_ity).to_json(), aes_leakage_model["byte"], "I(T,Y)")
database_inserts.save_success_rate_json(pd.Series(success_rate_all_epochs).to_json(), aes_leakage_model["byte"],
                                        "All epochs")
database_inserts.save_success_rate_json(pd.Series(success_rate_acc).to_json(), aes_leakage_model["byte"],
                                        "Val. Accuracy")
database_inserts.save_success_rate_json(pd.Series(success_rate_recall).to_json(), aes_leakage_model["byte"],
                                        "Val. Recall")
database_inserts.save_success_rate_json(pd.Series(success_rate_loss).to_json(), aes_leakage_model["byte"], "Val. Loss")
database_inserts.save_success_rate_json(pd.Series(success_rate_ity).to_json(), aes_leakage_model["byte"], "I(T,Y)")

# -------------------------------------------------------------------------------------------------------------------- #
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(rows=1, cols=2)
plots.new_plot()
list_of_series = [
    {"data": guessing_entropy_all_epochs, "label": "GE All Epochs", "color": "purple", "legend": True},
    {"data": guessing_entropy_acc, "label": "GE Accuracy", "color": "blue", "legend": True},
    {"data": guessing_entropy_recall, "label": "GE Recall", "color": "orange", "legend": True},
    {"data": guessing_entropy_loss, "label": "GE Loss", "color": "green", "legend": True},
    {"data": guessing_entropy_ity, "label": "GE I(T,Y)", "color": "red", "legend": True},
]
plots.create_line_plot(1, list_of_series, "Number of Traces", "Guessing Entropy", show_legend=True)

list_of_series = [
    {"data": success_rate_all_epochs, "label": "SR All Epochs", "color": "purple", "legend": True},
    {"data": success_rate_acc, "label": "SR Accuracy", "color": "blue", "legend": True},
    {"data": success_rate_recall, "label": "SR Recall", "color": "orange", "legend": True},
    {"data": success_rate_loss, "label": "SR Loss", "color": "green", "legend": True},
    {"data": success_rate_ity, "label": "SR I(T,Y)", "color": "red", "legend": True}
]
plots.create_line_plot(2, list_of_series, "Number of Traces", "Success Rate", show_legend=True)

plots.show_plot()

backend.clear_session()
