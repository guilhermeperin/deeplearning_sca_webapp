from flask import Flask, render_template
from commons.sca_database import ScaDatabase
from commons.sca_parameters import ScaParameters
from commons.sca_views import ScaViews
from database.tables import KeyRankJSON, SuccessRateJSON, Metric
from database.tables import Analysis, NeuralNetwork, TrainingHyperParameters, LeakageModel, HyperParameter
from plots.PlotlyPlots import PlotlyPlots
import os
import hiplot as hip
import time
from datetime import datetime
import flaskcode

app = Flask(__name__,
            static_url_path='',
            static_folder='webapp/static',
            template_folder='webapp/templates')

app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config.from_object(flaskcode.default_config)
app.config['FLASKCODE_RESOURCE_BASEPATH'] = 'scripts_aes'
app.register_blueprint(flaskcode.blueprint, url_prefix='/scripts_aes')


@app.before_request
def before_request():
    app.jinja_env.cache = {}


app.before_request(before_request)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/dashboard')
def dashboard():
    return render_template("dashboard/index.html")


@app.route('/scripts_aes')
def scripts_aes():
    return "ok"
    # dir_name = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # path = os.path.join(dir_name, 'deeplearning_sca_webapp\\scripts_aes')
    #
    # script_descriptions = ScriptDescription.script_descriptions()
    #
    # files = []
    # files_description = []
    # # r=root, d=directories, f = files
    # for r, d, f in os.walk(path):
    #     for file in f:
    #         if '.py' in file:
    #             files.append(file)
    #             files_description.append(script_descriptions[str(file).replace(".py", "")])
    #
    # return render_template("scripts.html", files=files, files_description=files_description)


@app.route('/tables')
def table():
    if os.path.exists("scripts_aes/database.sqlite"):

        db = ScaDatabase('scripts_aes/database.sqlite')
        analysis_all = db.select_all(Analysis)
        analyses = []

        for analysis in analysis_all:

            analysis_datetime = datetime.strptime(str(analysis.datetime), "%Y-%m-%d %H:%M:%S.%f").__format__(
                "%b %d, %Y %H:%M:%S")

            if analysis.model_fixed:
                final_key_ranks = db.select_final_key_rank_json_from_analysis(KeyRankJSON, analysis.id)
                final_success_rates = db.select_final_success_rate_from_analysis(SuccessRateJSON, analysis.id)
                neural_network = db.select_from_analysis(NeuralNetwork, analysis.id)
                analyses.append({
                    "id": analysis.id,
                    "script": analysis.script,
                    "datetime": analysis_datetime,
                    "elapsed_time": time.strftime('%H:%M:%S', time.gmtime(analysis.elapsed_time)),
                    "key_ranks": final_key_ranks,
                    "success_rates": final_success_rates,
                    "neural_network": neural_network.description,
                    "neural_network_name": neural_network.model_name,
                    "model_fixed": True,
                    "multiple_trainings": True if analysis.multiple_trainings else False
                })
            else:
                analyses.append({
                    "id": analysis.id,
                    "script": analysis.script,
                    "datetime": analysis_datetime,
                    "elapsed_time": time.strftime('%H:%M:%S', time.gmtime(analysis.elapsed_time)),
                    "key_ranks": 0,
                    "success_rates": 0,
                    "neural_network": "",
                    "neural_network_name": "",
                    "model_fixed": False,
                    "multiple_trainings": True if analysis.multiple_trainings else False
                })

        return render_template("tables.html", analyses=analyses)
    return render_template("tables.html", analyses=[])


@app.route('/script/<int:analysis_id>')
def script(analysis_id):
    db = ScaDatabase('scripts_aes/database.sqlite')

    analysis = db.select_analysis(Analysis, analysis_id)

    # get model information
    file_contents = ""

    dir_name = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_path = os.path.join(dir_name, 'deeplearning_sca_webapp\\scripts_aes')
    filename = file_path + "\\" + analysis.script

    file = open(filename, 'r')
    lines = file.readlines()
    for line in lines:
        file_contents += line

    return render_template("dashboard/script.html", file_contents=file_contents, script_file_name=analysis.script)


@app.route('/result/<int:analysis_id>/<plot_color>')
def result(analysis_id, plot_color):
    db = ScaDatabase('scripts_aes/database.sqlite')

    sca_views = ScaViews(analysis_id, plot_color)

    all_metric_plots = sca_views.metric_plots()
    all_key_rank_plots = sca_views.key_rank_plots()
    all_success_rate_plots = sca_views.success_rate_plots()

    # get neural network information from database
    neural_network_model = db.select_from_analysis(NeuralNetwork, analysis_id)

    # get training hyper-parameters information from database
    training_hyper_parameters = db.select_from_analysis(TrainingHyperParameters, analysis_id)

    # get leakage model information from database
    leakage_model = db.select_from_analysis(LeakageModel, analysis_id)

    # get model information
    file_contents = ""

    dir_name = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_path = os.path.join(dir_name, 'deeplearning_sca_webapp\\deep_learning_models')
    filename = file_path + "\\deeplearning.py"

    file = open(filename, 'r')
    lines = file.readlines()
    print_line = False
    for line in lines:

        if neural_network_model.model_name + "(" in line:
            print_line = True

        if print_line:
            file_contents += line

            if "return" in line:
                print_line = False

    return render_template("dashboard/result.html", all_plots=all_metric_plots, all_key_rank_plots=all_key_rank_plots,
                           all_success_rate_plots=all_success_rate_plots, file_contents=file_contents,
                           training_hyper_parameters=training_hyper_parameters, leakage_model=leakage_model)


@app.route('/result_hyper_parameter/<int:analysis_id>/<plot_color>')
def result_hyper_parameter(analysis_id, plot_color):
    db = ScaDatabase('scripts_aes/database.sqlite')

    plotly_plots = PlotlyPlots()

    hyper_parameters = db.select_from_analysis(HyperParameter, analysis_id=analysis_id)
    data = hyper_parameters.hyper_parameters
    exp = hip.Experiment().from_iterable(data)
    exp.display_data(hip.Displays.PARALLEL_PLOT).update({
        # Hide some columns
        'hide': ['uid'],
        # Specify the order for others
        'order': ['key_rank'],  # Put column time first on the left
    })
    exp.validate()
    exp.to_html("webapp/templates/hiplot.html")

    hp_dict = []
    hp_keys = data[0].keys()
    for key in hp_keys:
        hp_dict.append({
            "key": str(key),
            "data": [value[str(key)] for value in data]
        })

    hp_plots = []

    hp_item_x = None
    for hp_item in hp_dict:
        if hp_item['key'] == 'key_rank':
            hp_item_x = hp_item

    for hp_item_y in hp_dict:
        if hp_item_x['key'] != hp_item_y['key']:
            hp_plots.append({
                # "plot": plotly_plots.create_scatter_plot(x=hp_item_x['data'], y=hp_item_y['data'], z=hp_item_x['data'],
                #                             line_name=hp_item_x['key'] + " vs " + hp_item_y['key']),
                "plot": plotly_plots.create_scatter_hist_plot(x=hp_item_x['data'], y=hp_item_y['data'],
                                                              z=hp_item_x['data'],
                                                              line_name=hp_item_x['key'] + " vs " + hp_item_y['key']),
                "title": hp_item_x['key'] + " vs " + hp_item_y['key'],
                # "layout_plotly": plotly_plots.get_plotly_layout(hp_item_x['key'], hp_item_y['key'], plot_color)
                "layout_plotly": plotly_plots.get_layout_density(hp_item_x['key'], hp_item_y['key'], plot_color)
            })

    return render_template("dashboard/result_hyper_parameters.html", hp_plots=hp_plots)


@app.route('/hyper_parameters_search')
def result_search():
    db = ScaDatabase('scripts_aes/database.sqlite')

    plotly_plots = PlotlyPlots()

    plot_color = "lightgrey"

    all_key_ranks = []
    hyper_parameters_all = db.select_all(HyperParameter)
    for hyper_parameter in hyper_parameters_all:
        key_ranks = []
        for hp in hyper_parameter.hyper_parameters:
            key_ranks.append(hp['key_rank'])
        all_key_ranks.append({
            "analysis_id": hyper_parameter.analysis_id,
            "plot": plotly_plots.create_density_plot(x=key_ranks, line_name="key_rank"),
            "title": "Key Rank",
            "layout_plotly": plotly_plots.get_layout_histogram("Key Rank", "Density", plot_color)
        })

    return render_template("dashboard/result_search.html", all_key_ranks=all_key_ranks)


if __name__ == '__main__':
    app.run(debug=True)
