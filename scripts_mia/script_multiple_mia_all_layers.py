import numpy as np
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import CalculateMutualInformationAllLayers
from commons.sca_datasets import ScaDataSets
from commons.sca_plots import Plots

# import os
# import tensorflow as tf
#
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
#
# if tf.test.gpu_device_name():
#     print('GPU found')
# else:
#     print("No GPU found")

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'D:/traces/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("dpa_v4")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 0-10
#  "round_state_output": 0-10
#  "state_input": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  "state_output": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW, HD, ID, bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#

aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "bit",
    "bit": 7,
    "byte": 0,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#

data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, train_dataset, validation_dataset = data_sets.load_training_and_validation_sets()
x_test_fixed_key, x_validation_fixed_key, trace_data_test_fixed_key, trace_data_validation_fixed_key = data_sets.load_two_tests(
    random_order=True)

# ---------------------------------------------------------------------------------------------------------------------#
#  Create categorical labels from selected leakage model and cipher
#  A categorical label is a vector with all zeros, except at the index of the class/label
#  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
# ---------------------------------------------------------------------------------------------------------------------#

crypto = AES()
train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
validation_labels_fixed_key = crypto.create_labels(trace_data_validation_fixed_key, aes_leakage_model, param)
test_labels_fixed_key = crypto.create_labels(trace_data_test_fixed_key, aes_leakage_model, param)

y_train = to_categorical(train_labels, num_classes=param["classes"])
y_validation = to_categorical(validation_labels, num_classes=param["classes"])
y_validation_fixed_key = to_categorical(validation_labels_fixed_key, num_classes=param["classes"])
y_test_fixed_key = to_categorical(test_labels_fixed_key, num_classes=param["classes"])

number_of_runs = 2

model_obj = DeepLearningModel()
model = model_obj.basic_mlp_ixt_ity(param["classes"], param["number_of_samples"])

mia_layers_ity = np.zeros((len(model.layers) - 1, param["epochs"]))
mia_layers_ixt = np.zeros((len(model.layers) - 1, param["epochs"]))

for run in range(number_of_runs):

    print("run " + str(run))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    # callback_mia_validation_all_layers_ity = CalculateMutualInformationAllLayers(validation_dataset, y_validation, 30)
    # callback_mia_validation_all_layers_ixt = CalculateMutualInformationAllLayers(validation_dataset, y_validation, 30,
    #                                                                              ixt=True)
    callback_mia_validation_all_layers_ity = CalculateMutualInformationAllLayers(train_dataset[0:param["n_validation"]],
                                                                                 y_train[0:param["n_validation"]], 30)
    callback_mia_validation_all_layers_ixt = CalculateMutualInformationAllLayers(train_dataset[0:param["n_validation"]],
                                                                                 y_train[0:param["n_validation"]], 30,
                                                                                 ixt=True)

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    # -----------------------------------------------------------------------------------------------------------------#

    model_obj = DeepLearningModel()
    model = model_obj.basic_mlp_ixt_ity(param["classes"], param["number_of_samples"])
    model.fit(x=x_train,
              y=y_train,
              batch_size=400,
              verbose=1,
              epochs=param["epochs"],
              shuffle=True,
              validation_data=(x_validation, y_validation),
              callbacks=[callback_mia_validation_all_layers_ity, callback_mia_validation_all_layers_ixt])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Retrieve I(X,T) and I(T,Y) for all layers
    # -----------------------------------------------------------------------------------------------------------------#

    mia_all_layers_ity = callback_mia_validation_all_layers_ity.get_mia()
    mia_all_layers_ixt = callback_mia_validation_all_layers_ixt.get_mia()

    for epoch in range(len(mia_all_layers_ity)):
        for layer in range(len(mia_all_layers_ity[epoch])):
            mia_layers_ity[layer][epoch] += mia_all_layers_ity[epoch]['l_' + str(layer + 1)]

    for epoch in range(len(mia_all_layers_ixt)):
        for layer in range(len(mia_all_layers_ixt[epoch])):
            mia_layers_ixt[layer][epoch] += mia_all_layers_ixt[epoch]['l_' + str(layer + 1)]

    backend.clear_session()

# ---------------------------------------------------------------------------------------------------------------------#
#  Compute average I(X,T) and I(T,Y)
# ---------------------------------------------------------------------------------------------------------------------#

for epoch in range(param["epochs"]):
    for layer in range(len(model.layers) - 1):
        mia_layers_ity[layer][epoch] /= number_of_runs

for epoch in range(param["epochs"]):
    for layer in range(len(model.layers) - 1):
        mia_layers_ixt[layer][epoch] /= number_of_runs

# ---------------------------------------------------------------------------------------------------------------------#
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

# plots = Plots(1, 1)
# plots.new_plot()
#
# values = range(param["epochs"])
# jet = cm = plots.get_color_map()
# cNorm = colors.Normalize(vmin=0, vmax=values[-1])
# scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)
#
# for layer in range(len(model.layers) - 1):
#     list_of_series = []
#     for epoch in range(param["epochs"]):
#         colorVal = scalarMap.to_rgba(epoch)
#         list_of_series.append(
#             {"data_x": mia_layers_ixt[layer][epoch], "data_y": mia_layers_ity[layer][epoch],
#              "label": "Layer " + str(layer), "color": colorVal, "legend": False}
#         )
#     plots.create_scatter_plot(1, list_of_series, "I(X,T)", "I(T,Y)", show_legend=False)
#
# plots.show_plot()

plots = Plots(1, 4)
plots.new_plot()

for layer in range(len(model.layers) - 1):
    list_of_series = []
    for epoch in range(1, 2):
        list_of_series.append(
            {"data_x": mia_layers_ixt[layer][epoch], "data_y": mia_layers_ity[layer][epoch],
             "label": "Layer " + str(layer), "color": "blue", "legend": False}
        )
    plots.create_scatter_plot(1, list_of_series, "I(X,T)", "I(T,Y)", show_legend=False)

    list_of_series = []
    for epoch in range(19, 20):
        list_of_series.append(
            {"data_x": mia_layers_ixt[layer][epoch], "data_y": mia_layers_ity[layer][epoch],
             "label": "Layer " + str(layer), "color": "green", "legend": False}
        )
    plots.create_scatter_plot(2, list_of_series, "I(X,T)", "I(T,Y)", show_legend=False)

    list_of_series = []
    for epoch in range(99, 100):
        list_of_series.append(
            {"data_x": mia_layers_ixt[layer][epoch], "data_y": mia_layers_ity[layer][epoch],
             "label": "Layer " + str(layer), "color": "orange", "legend": False}
        )
    plots.create_scatter_plot(3, list_of_series, "I(X,T)", "I(T,Y)", show_legend=False)

    list_of_series = []
    for epoch in range(199, 200):
        list_of_series.append(
            {"data_x": mia_layers_ixt[layer][epoch], "data_y": mia_layers_ity[layer][epoch],
             "label": "Layer " + str(layer), "color": "red", "legend": False}
        )
    plots.create_scatter_plot(4, list_of_series, "I(X,T)", "I(T,Y)", show_legend=False)

plots.show_plot()

backend.clear_session()
