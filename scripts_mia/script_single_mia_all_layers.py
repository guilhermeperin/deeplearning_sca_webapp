import numpy as np
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import CalculateMutualInformationAllLayers
from commons.sca_datasets import ScaDataSets
from commons.sca_plots import Plots
import matplotlib.colors as colors
import matplotlib.cm as cmx
import time
# import os
# import tensorflow as tf
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
#
# if tf.test.gpu_device_name():
#     print('GPU found')
# else:
#     print("No GPU found")

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#
trace_directory_path = 'D:/traces/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("ches_ctf")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 0-10
#  "round_state_output": 0-10
#  "state_input": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  "state_output": SubBytesIn, SubBytesOut, AddRoundKeyIn, AddRoundKeyOut, ShiftRowsIn, ShiftRowsOut, MixColumnsIn,
#  MixColumnsOut, InvSubBytesIn, InvSubBytesOut, InvAddRoundKeyIn, InvAddRoundKeyOut, InvShiftRowsIn, InvShiftRowsOut,
#  InvMixColumnsIn, InvMixColumnsOut
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW, HD, ID, bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#
aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "HW",
    "bit": 0,
    "byte": 0,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#
data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, train_dataset, validation_dataset, _, _ = data_sets.load_training_and_validation_sets()
x_test_fixed_key, x_validation_fixed_key, trace_data_test_fixed_key, trace_data_validation_fixed_key = data_sets.load_two_tests(
    random_order=True)

# ---------------------------------------------------------------------------------------------------------------------#
#  Create categorical labels from selected leakage model and cipher
#  A categorical label is a vector with all zeros, except at the index of the class/label
#  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
# ---------------------------------------------------------------------------------------------------------------------#
crypto = AES()
train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
validation_labels_fixed_key = crypto.create_labels(trace_data_validation_fixed_key, aes_leakage_model, param)
test_labels_fixed_key = crypto.create_labels(trace_data_test_fixed_key, aes_leakage_model, param)

y_train = to_categorical(train_labels, num_classes=param["classes"])
y_validation = to_categorical(validation_labels, num_classes=param["classes"])
y_validation_fixed_key = to_categorical(validation_labels_fixed_key, num_classes=param["classes"])
y_test_fixed_key = to_categorical(test_labels_fixed_key, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Create labels for each key byte hypothesis for test set in order to calculate key ranking
# ---------------------------------------------------------------------------------------------------------------------#

number_of_traces_test = int(param["n_test"] / 2)
labels_key_hypothesis_test_fixed_key = np.zeros((param["number_of_key_hypothesis"], number_of_traces_test))
labels_key_hypothesis_validation_fixed_key = np.zeros((param["number_of_key_hypothesis"], number_of_traces_test))

for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
    key_h = bytearray.fromhex(param["key"])
    key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
    crypto.set_key(key_h)
    labels_key_hypothesis_test_fixed_key[key_byte_hypothesis][:] = crypto.create_labels(
        trace_data_test_fixed_key, aes_leakage_model, param, key_hypothesis=True
    )
    labels_key_hypothesis_validation_fixed_key[key_byte_hypothesis][:] = crypto.create_labels(
        trace_data_validation_fixed_key, aes_leakage_model, param, key_hypothesis=True
    )

# ---------------------------------------------------------------------------------------------------------------------#
#  Declare callbacks
# ---------------------------------------------------------------------------------------------------------------------#
x_t, y_t = train_dataset[0:param["n_validation"]], y_train[0:param["n_validation"]]
callback_mia_validation_all_layers_ity = CalculateMutualInformationAllLayers(x_t, y_t, 100)
callback_mia_validation_all_layers_ixt = CalculateMutualInformationAllLayers(x_t, y_t, 100, ixt=True)

# ---------------------------------------------------------------------------------------------------------------------#
#  Declare neural network model and run training by calling .fit()
# ---------------------------------------------------------------------------------------------------------------------#
model_obj = DeepLearningModel()
model_name_obj = model_obj.basic_mlp_ixt_ity_relu
model = model_name_obj(param["classes"], param["number_of_samples"])
start = time.time()
history = model.fit(x=x_train,
                    y=y_train,
                    batch_size=param["mini-batch"],
                    verbose=1,
                    epochs=param["epochs"],
                    shuffle=True,
                    validation_data=(x_validation, y_validation),
                    callbacks=[callback_mia_validation_all_layers_ity, callback_mia_validation_all_layers_ixt])
elapsed_time = time.time() - start

# ---------------------------------------------------------------------------------------------------------------------#
#  Retrieve I(X,T) and I(T,Y) for all layers
# ---------------------------------------------------------------------------------------------------------------------#

mia_all_layers_ity = callback_mia_validation_all_layers_ity.get_mia()
mia_all_layers_ixt = callback_mia_validation_all_layers_ixt.get_mia()

mia_layers_ity = np.zeros((len(mia_all_layers_ity[0]), param["epochs"]))
mia_layers_ixt = np.zeros((len(mia_all_layers_ixt[0]), param["epochs"]))

for epoch in range(len(mia_all_layers_ity)):
    for layer in range(len(mia_all_layers_ity[epoch])):
        mia_layers_ity[layer][epoch] = mia_all_layers_ity[epoch]['l_' + str(layer + 1)]

for epoch in range(len(mia_all_layers_ixt)):
    for layer in range(len(mia_all_layers_ixt[epoch])):
        mia_layers_ixt[layer][epoch] = mia_all_layers_ixt[epoch]['l_' + str(layer + 1)]

print(mia_layers_ixt)
print(mia_layers_ity)

# ---------------------------------------------------------------------------------------------------------------------#
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(1, 1)
plots.new_plot()

values = range(len(mia_all_layers_ity[0]))
jet = cm = plots.get_color_map()
cNorm = colors.Normalize(vmin=0, vmax=values[-1])
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

for layer in range(len(mia_all_layers_ity[0])):
    colorVal = scalarMap.to_rgba(layer)
    list_of_series = [
        {"data_x": mia_layers_ixt[layer], "data_y": mia_layers_ity[layer], "label": "Layer " + str(layer),
         "color": colorVal, "legend": True}
    ]
    plots.create_scatter_plot(1, list_of_series, "I(X,T)", "I(T,Y)", show_legend=True)

plots.show_plot()

backend.clear_session()
