import numpy as np
import random
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import CalculateAccuracyCurve25519
from commons.sca_datasets import ScaDataSets
from commons.sca_plots import Plots
import matplotlib.colors as colors
import matplotlib.cm as cmx
from scipy import stats

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'G:/DL_traces/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("ecc_ha_winres")

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#

data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, y_train_data, y_validation_data, _, _, trace_samples, trace_data = data_sets.load_training_and_validation_sets()
x_test, y_test_data, _ = data_sets.load_test_set()

# ---------------------------------------------------------------------------------------------------------------------#
#  Get trace data/labels as categorical values (e.g., label 0 = [1, 0], label 1 = [0, 1])
#  traces data contains two bytes:
#  byte 0: labels obtained after applying an horizontal attack
#  byte 1: true labels from correct key
# ---------------------------------------------------------------------------------------------------------------------#

labels_from_pkc_data_train_ha = [0 if row[0] == 0 else 1 for row in y_train_data]
labels_from_pkc_data_train_true = [0 if row[1] == 0 else 1 for row in y_train_data]
labels_from_pkc_data_validation_ha = [0 if row[0] == 0 else 1 for row in y_validation_data]
labels_from_pkc_data_validation_true = [0 if row[1] == 0 else 1 for row in y_validation_data]
labels_from_pkc_data_test_ha = [0 if row[0] == 0 else 1 for row in y_test_data]
labels_from_pkc_data_test_true = [0 if row[1] == 0 else 1 for row in y_test_data]

y_train = to_categorical(labels_from_pkc_data_train_ha, num_classes=param["classes"])
y_validation = to_categorical(labels_from_pkc_data_validation_ha, num_classes=param["classes"])
y_test = to_categorical(labels_from_pkc_data_test_true, num_classes=param["classes"])

labels_from_pkc_data_train_ha_relabel = np.zeros(param["n_train"])
labels_from_pkc_data_validation_ha_relabel = np.zeros(param["n_validation"])

x_train_validation_ha_shuffle = np.empty(
    shape=(param["n_train"] + param["n_validation"], param["number_of_samples"]),
    dtype="float32")

for i in range(param["n_train"] + param["n_validation"]):
    x_train_validation_ha_shuffle[i, :] = trace_samples[i]

# ---------------------------------------------------------------------------------------------------------------------#
#  T-Test based on HA
# ---------------------------------------------------------------------------------------------------------------------#

labels_from_pkc_data_ha = np.zeros(param["n_train"] + param["n_validation"])
for i in range(param["n_train"]):
    labels_from_pkc_data_ha[i] = labels_from_pkc_data_train_ha[i]

for i in range(param["n_validation"]):
    labels_from_pkc_data_ha[i + param["n_train"]] = labels_from_pkc_data_validation_ha[i]

n1 = sum(labels_from_pkc_data_ha)
n0 = param["n_train"] + param["n_validation"] - n1

set1 = np.zeros((int(n1), param["number_of_samples"]))
set0 = np.zeros((int(n0), param["number_of_samples"]))

n0_count = 0
n1_count = 0
for i in range(int(n0 + n1)):
    if labels_from_pkc_data_ha[i] == 0:
        set0[n0_count] = trace_samples[i]
        n0_count += 1
    else:
        set1[n1_count] = trace_samples[i]
        n1_count += 1

t_test_ha = stats.ttest_ind(set0, set1)[0]

# ---------------------------------------------------------------------------------------------------------------------#
#  Correct some HA labels
# ---------------------------------------------------------------------------------------------------------------------#

# for i in range(param["n_train"]):
#     r = random.randint(0, 9)
#     if r == 0:
#         labels_from_pkc_data_train_ha[i] = labels_from_pkc_data_train_true[i]
#
# for i in range(param["n_validation"]):
#     r = random.randint(0, 9)
#     if r == 0:
#         labels_from_pkc_data_validation_ha[i] = labels_from_pkc_data_validation_true[i]
#
# y_train = to_categorical(labels_from_pkc_data_train_ha, num_classes=param["classes"])
# y_validation = to_categorical(labels_from_pkc_data_validation_ha, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Get amount of correct bits from HA labels
# ---------------------------------------------------------------------------------------------------------------------#

correct_bits_training_set = 0
for index in range(len(labels_from_pkc_data_train_true)):
    if labels_from_pkc_data_train_true[index] == labels_from_pkc_data_train_ha[index]:
        correct_bits_training_set += 1

correct_bits_test_set = 0
for index in range(len(labels_from_pkc_data_test_true)):
    if labels_from_pkc_data_test_true[index] == labels_from_pkc_data_test_ha[index]:
        correct_bits_test_set += 1

print("Correct rate for training set: " + str(correct_bits_training_set / len(labels_from_pkc_data_train_true)))
print("Correct rate for test set: " + str(correct_bits_test_set / len(labels_from_pkc_data_test_true)))

# ---------------------------------------------------------------------------------------------------------------------#
#  Train multiple deep_learning_models to generate an ensemble
# ---------------------------------------------------------------------------------------------------------------------#

number_of_models = 20
number_of_bits_per_test_trace = 255
number_of_validation_traces = int(len(labels_from_pkc_data_test_true) / number_of_bits_per_test_trace)
output_probabilities = np.zeros((param["n_test"], 2))

# accuracy_training = np.zeros((number_of_models, param["epochs"]))
# accuracy_test = np.zeros((number_of_models, param["epochs"]))
#
# recall_training = np.zeros((number_of_models, param["epochs"]))
# recall_test = np.zeros((number_of_models, param["epochs"]))
#
# loss_training = np.zeros((number_of_models, param["epochs"]))
# loss_validation = np.zeros((number_of_models, param["epochs"]))

accuracy_dl_set1 = np.zeros((number_of_models, param["epochs"], number_of_validation_traces))
accuracy_ha_set1 = np.zeros((number_of_models, param["epochs"], number_of_validation_traces))

accuracy_dl_set2 = np.zeros((number_of_models, param["epochs"], number_of_validation_traces))
accuracy_ha_set2 = np.zeros((number_of_models, param["epochs"], number_of_validation_traces))

max_accuracy = 0

t_test = np.zeros((number_of_models, param["number_of_samples"]))

for model_index in range(number_of_models):

    print("Model: " + str(model_index))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    # callbacks_training = TrainingCallback(x_train, y_train)
    # callbacks_test = ValidationCallback(x_test, y_test)
    callbacks_pkc_curve25519 = CalculateAccuracyCurve25519(x_test, labels_from_pkc_data_test_true,
                                                           labels_from_pkc_data_test_ha, param["epochs"])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    #  Train on training data and relabel validation data
    # -----------------------------------------------------------------------------------------------------------------#

    model_obj = DeepLearningModel()
    model_set_1 = model_obj.basic_cnn_random_ha(param["classes"], param["number_of_samples"])
    model_set_1.fit(x=x_train,
                    y=y_train,
                    batch_size=64,
                    verbose=1,
                    epochs=param["epochs"],
                    shuffle=True,
                    validation_data=(x_test, y_test),
                    callbacks=[callbacks_pkc_curve25519])

    output_probabilities = model_set_1.predict(x_validation)

    y_validation_relabel = np.zeros(len(output_probabilities))
    for index in range(len(output_probabilities)):
        if output_probabilities[index][0] > 0.5:
            y_validation_relabel[index] = 0
            labels_from_pkc_data_validation_ha_relabel[index] = 0
        else:
            y_validation_relabel[index] = 1
            labels_from_pkc_data_validation_ha_relabel[index] = 1

    y_validation = to_categorical(y_validation_relabel, num_classes=param["classes"])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Get amount of correct bits from HA labels
    # -----------------------------------------------------------------------------------------------------------------#

    correct_bits_training_set = 0
    for index in range(len(labels_from_pkc_data_validation_true)):
        if labels_from_pkc_data_validation_true[index] == y_validation_relabel[index]:
            correct_bits_training_set += 1

    print(
        "Correct rate for training set: " + str(correct_bits_training_set / len(labels_from_pkc_data_validation_true)))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    # accuracy_training[model_index] = callbacks_training.get_accuracy()
    # accuracy_test[model_index] = callbacks_test.get_accuracy()
    #
    # recall_training[model_index] = callbacks_training.get_recall()
    # recall_test[model_index] = callbacks_test.get_recall()
    #
    # loss_training[model_index] = callbacks_training.get_loss()
    # loss_validation[model_index] = callbacks_test.get_loss()

    accuracy_dl_set1[model_index] = callbacks_pkc_curve25519.get_correct_dl()
    accuracy_ha_set1[model_index] = callbacks_pkc_curve25519.get_correct_ha()
    max_accuracy_all_epochs = callbacks_pkc_curve25519.get_max_accuracy()

    if max_accuracy_all_epochs > max_accuracy:
        max_accuracy = max_accuracy_all_epochs

    print("\nMax Accuracy: " + str(max_accuracy))

    backend.clear_session()

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    # callbacks_training = TrainingCallback(x_train, y_train)
    # callbacks_test = ValidationCallback(x_test, y_test)
    callbacks_pkc_curve25519 = CalculateAccuracyCurve25519(x_test, labels_from_pkc_data_test_true,
                                                           labels_from_pkc_data_test_ha, param["epochs"])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    #  Train on validation data and relabel training data
    # -----------------------------------------------------------------------------------------------------------------#

    model_obj = DeepLearningModel()
    model_set_2 = model_obj.basic_cnn_random_ha(param["classes"], param["number_of_samples"])
    model_set_2.fit(x=x_validation,
                    y=y_validation,
                    batch_size=64,
                    verbose=1,
                    epochs=param["epochs"],
                    shuffle=True,
                    validation_data=(x_test, y_test),
                    callbacks=[callbacks_pkc_curve25519])

    output_probabilities = model_set_2.predict(x_train)

    y_train_relabel = np.zeros(len(output_probabilities))
    for index in range(len(output_probabilities)):
        if output_probabilities[index][0] > 0.5:
            y_train_relabel[index] = 0
            labels_from_pkc_data_train_ha_relabel[index] = 0
        else:
            y_train_relabel[index] = 1
            labels_from_pkc_data_train_ha_relabel[index] = 1

    y_train = to_categorical(y_train_relabel, num_classes=param["classes"])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Get amount of correct bits from HA labels
    # -----------------------------------------------------------------------------------------------------------------#

    correct_bits_training_set = 0
    for index in range(len(labels_from_pkc_data_train_true)):
        if labels_from_pkc_data_train_true[index] == y_train_relabel[index]:
            correct_bits_training_set += 1

    print("Correct rate for training set: " + str(correct_bits_training_set / len(labels_from_pkc_data_train_true)))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    # accuracy_training[model_index] = callbacks_training.get_accuracy()
    # accuracy_test[model_index] = callbacks_test.get_accuracy()
    #
    # recall_training[model_index] = callbacks_training.get_recall()
    # recall_test[model_index] = callbacks_test.get_recall()
    #
    # loss_training[model_index] = callbacks_training.get_loss()
    # loss_validation[model_index] = callbacks_test.get_loss()

    accuracy_dl_set2[model_index] = callbacks_pkc_curve25519.get_correct_dl()
    accuracy_ha_set2[model_index] = callbacks_pkc_curve25519.get_correct_ha()
    max_accuracy_all_epochs = callbacks_pkc_curve25519.get_max_accuracy()

    if max_accuracy_all_epochs > max_accuracy:
        max_accuracy = max_accuracy_all_epochs

    print("\nMax Accuracy: " + str(max_accuracy))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Shuffle Train and Validation Sets
    # -----------------------------------------------------------------------------------------------------------------#

    random_list = list(range(0, param["n_train"] + param["n_validation"]))
    random.shuffle(random_list)

    labels_from_pkc_data_ha_relabel = np.zeros(param["n_train"] + param["n_validation"])
    for i in range(param["n_train"]):
        labels_from_pkc_data_ha_relabel[i] = labels_from_pkc_data_train_ha_relabel[i]

    for i in range(param["n_validation"]):
        labels_from_pkc_data_ha_relabel[i + param["n_train"]] = labels_from_pkc_data_validation_ha_relabel[i]

    labels_from_pkc_data_ha_shuffle = np.zeros(param["n_train"] + param["n_validation"])

    labels_from_pkc_data_true = np.zeros(param["n_train"] + param["n_validation"])
    for i in range(param["n_train"]):
        labels_from_pkc_data_true[i] = labels_from_pkc_data_train_true[i]

    for i in range(param["n_validation"]):
        labels_from_pkc_data_true[i + param["n_train"]] = labels_from_pkc_data_validation_true[i]

    labels_from_pkc_data_true_shuffle = np.zeros(param["n_train"] + param["n_validation"])

    x_train_validation_ha_shuffle_temp = np.empty(
        shape=(param["n_train"] + param["n_validation"], param["number_of_samples"]),
        dtype="float32")

    for i in range(param["n_train"] + param["n_validation"]):
        x_train_validation_ha_shuffle_temp[i, :] = x_train_validation_ha_shuffle[i]

    for i in range(param["n_train"] + param["n_validation"]):
        x_train_validation_ha_shuffle[i, :] = x_train_validation_ha_shuffle_temp[random_list[i]]
        labels_from_pkc_data_ha_shuffle[i] = labels_from_pkc_data_ha_relabel[random_list[i]]
        labels_from_pkc_data_true_shuffle[i] = labels_from_pkc_data_true[random_list[i]]

    labels_from_pkc_data_train_ha = labels_from_pkc_data_ha_shuffle[0: param["n_train"]]
    labels_from_pkc_data_validation_ha = labels_from_pkc_data_ha_shuffle[
                                         param["n_train"]: param["n_train"] + param["n_validation"]]

    labels_from_pkc_data_train_true = labels_from_pkc_data_true_shuffle[0: param["n_train"]]
    labels_from_pkc_data_validation_true = labels_from_pkc_data_true_shuffle[
                                           param["n_train"]: param["n_train"] + param["n_validation"]]

    y_train = to_categorical(labels_from_pkc_data_train_ha, num_classes=param["classes"])
    y_validation = to_categorical(labels_from_pkc_data_validation_ha, num_classes=param["classes"])

    training_dataset_reshaped = x_train_validation_ha_shuffle.reshape(
        (x_train_validation_ha_shuffle.shape[0], x_train_validation_ha_shuffle.shape[1], 1))

    x_train = training_dataset_reshaped[0:param["n_train"]]
    x_validation = training_dataset_reshaped[param["n_train"]:param["n_train"] + param["n_validation"]]

    n1 = sum(labels_from_pkc_data_ha_shuffle)
    n0 = param["n_train"] + param["n_validation"] - n1

    set1 = np.zeros((int(n1), param["number_of_samples"]))
    set0 = np.zeros((int(n0), param["number_of_samples"]))

    n0_count = 0
    n1_count = 0
    for i in range(int(n0 + n1)):
        if labels_from_pkc_data_ha_shuffle[i] == 0:
            set0[n0_count] = x_train_validation_ha_shuffle[i]
            n0_count += 1
        else:
            set1[n1_count] = x_train_validation_ha_shuffle[i]
            n1_count += 1

    t_test[model_index] = stats.ttest_ind(set0, set1)[0]

    backend.clear_session()

# ---------------------------------------------------------------------------------------------------------------------#
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(2, 2)
plots.new_plot()

values = range(number_of_models)
jet = cm = plots.get_color_map()
cNorm = colors.Normalize(vmin=0, vmax=values[-1])
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

list_of_series = []
for model_index in range(number_of_models):

    accuracy_avg = np.zeros(param["epochs"])
    for trace_index in range(number_of_validation_traces):
        accuracy_epoch = np.zeros(param["epochs"])
        for epoch in range(param["epochs"]):
            accuracy_epoch[epoch] = accuracy_dl_set1[model_index][epoch][trace_index]
        accuracy_avg += accuracy_epoch
    accuracy_avg /= number_of_validation_traces

    colorVal = scalarMap.to_rgba(model_index)
    list_of_series.append(
        {"data": accuracy_avg, "label": "HA + CNN", "color": colorVal, "legend": False})
    # list_of_series.append({"data": accuracy_ha_set1[model_index], "label": "HA", "color": "orange", "legend": False})
plots.create_line_plot(1, list_of_series, "Single Traces", "Accuracy", show_legend=True)

list_of_series = []
for model_index in range(number_of_models):

    accuracy_avg = np.zeros(param["epochs"])
    for trace_index in range(number_of_validation_traces):
        accuracy_epoch = np.zeros(param["epochs"])
        for epoch in range(param["epochs"]):
            accuracy_epoch[epoch] = accuracy_dl_set2[model_index][epoch][trace_index]
        accuracy_avg += accuracy_epoch
    accuracy_avg /= number_of_validation_traces

    colorVal = scalarMap.to_rgba(model_index)
    list_of_series.append(
        {"data": accuracy_avg, "label": "HA + CNN", "color": colorVal, "legend": False})
    # list_of_series.append({"data": accuracy_ha_set2[model_index], "label": "HA", "color": "red", "legend": False})
plots.create_line_plot(2, list_of_series, "Single Traces", "Accuracy", show_legend=True)

list_of_series = []
list_of_series.append({"data": t_test_ha, "label": "t-test", "color": "black", "legend": False})
for model_index in range(number_of_models):
    colorVal = scalarMap.to_rgba(model_index)
    list_of_series.append(
        {"data": t_test[model_index], "label": "t-test", "color": colorVal, "legend": False})
plots.create_line_plot(3, list_of_series, "Samples", "T-Test", show_legend=True)

plots.show_plot()

backend.clear_session()
