import numpy as np
from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import CalculateAccuracyCurve25519
from commons.sca_datasets import ScaDataSets
from commons.sca_plots import Plots

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'D:/traces/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("ecc_ha_winres")

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#

data_sets = ScaDataSets(param, trace_directory_path)
x_train, _, y_train_data, _, _, _ = data_sets.load_training_and_validation_sets()
x_test, y_test_data, _ = data_sets.load_test_set()

# ---------------------------------------------------------------------------------------------------------------------#
#  Get trace data/labels as categorical values (e.g., label 0 = [1, 0], label 1 = [0, 1])
#  traces data contains two bytes:
#  byte 0: labels obtained after applying an horizontal attack
#  byte 1: true labels from correct key
# ---------------------------------------------------------------------------------------------------------------------#

labels_from_pkc_data_train_ha = [0 if row[0] == 0 else 1 for row in y_train_data]
labels_from_pkc_data_train_true = [0 if row[1] == 0 else 1 for row in y_train_data]
labels_from_pkc_data_test_ha = [0 if row[0] == 0 else 1 for row in y_test_data]
labels_from_pkc_data_test_true = [0 if row[1] == 0 else 1 for row in y_test_data]

y_train = to_categorical(labels_from_pkc_data_train_ha, num_classes=param["classes"])
y_test = to_categorical(labels_from_pkc_data_test_true, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Get amount of correct bits from HA labels
# ---------------------------------------------------------------------------------------------------------------------#

correct_bits_training_set = 0
for index in range(len(labels_from_pkc_data_train_true)):
    if labels_from_pkc_data_train_true[index] == labels_from_pkc_data_train_ha[index]:
        correct_bits_training_set += 1

correct_bits_test_set = 0
for index in range(len(labels_from_pkc_data_test_true)):
    if labels_from_pkc_data_test_true[index] == labels_from_pkc_data_test_ha[index]:
        correct_bits_test_set += 1

print("Correct rate for training set: " + str(correct_bits_training_set / len(labels_from_pkc_data_train_true)))
print("Correct rate for test set: " + str(correct_bits_test_set / len(labels_from_pkc_data_test_true)))

# ---------------------------------------------------------------------------------------------------------------------#
#  Train multiple deep_learning_models to generate an ensemble
# ---------------------------------------------------------------------------------------------------------------------#

number_of_models = 10
number_of_bits_per_test_trace = 255
number_of_validation_traces = int(len(labels_from_pkc_data_test_true) / number_of_bits_per_test_trace)

# accuracy_training = np.zeros((number_of_models, param["epochs"]))
# accuracy_test = np.zeros((number_of_models, param["epochs"]))
#
# recall_training = np.zeros((number_of_models, param["epochs"]))
# recall_test = np.zeros((number_of_models, param["epochs"]))
#
# loss_training = np.zeros((number_of_models, param["epochs"]))
# loss_validation = np.zeros((number_of_models, param["epochs"]))

accuracy_dl = np.zeros((number_of_models, number_of_validation_traces))
accuracy_ha = np.zeros((number_of_models, number_of_validation_traces))

max_accuracy = 0

for model_index in range(number_of_models):

    print("Model: " + str(model_index))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    # callbacks_training = TrainingCallback(x_train, y_train)
    # callbacks_test = ValidationCallback(x_test, y_test)
    callbacks_pkc_curve25519 = CalculateAccuracyCurve25519(x_test, labels_from_pkc_data_test_true,
                                                           labels_from_pkc_data_test_ha, param["epochs"])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    # -----------------------------------------------------------------------------------------------------------------#

    model_obj = DeepLearningModel()
    model = model_obj.basic_cnn_random(param["classes"], param["number_of_samples"])
    model.fit(x=x_train,
              y=y_train,
              batch_size=64,
              verbose=1,
              epochs=param["epochs"],
              shuffle=True,
              validation_data=(x_test, y_test),
              callbacks=[callbacks_pkc_curve25519])

    # -----------------------------------------------------------------------------------------------------------------#
    #  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    # accuracy_training[model_index] = callbacks_training.get_accuracy()
    # accuracy_test[model_index] = callbacks_test.get_accuracy()
    #
    # recall_training[model_index] = callbacks_training.get_recall()
    # recall_test[model_index] = callbacks_test.get_recall()
    #
    # loss_training[model_index] = callbacks_training.get_loss()
    # loss_validation[model_index] = callbacks_test.get_loss()

    accuracy_dl[model_index] = callbacks_pkc_curve25519.get_correct_dl()[param["epochs"] - 1]
    accuracy_ha[model_index] = callbacks_pkc_curve25519.get_correct_ha()[param["epochs"] - 1]
    max_accuracy_all_epochs = callbacks_pkc_curve25519.get_max_accuracy()

    if max_accuracy_all_epochs > max_accuracy:
        max_accuracy = max_accuracy_all_epochs

    print("\nMax Accuracy: " + str(max_accuracy))

    backend.clear_session()

# ---------------------------------------------------------------------------------------------------------------------#
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(1, 1)
plots.new_plot()

# list_of_series = []
# for model_index in range(number_of_models):
#     list_of_series.append(
#         {"data": accuracy_training[model_index], "label": "Training", "color": "grey", "legend": False})
#     list_of_series.append({"data": accuracy_test[model_index], "label": "Test", "color": "orange", "legend": False})
# plots.create_line_plot(1, list_of_series, "Epochs", "Accuracy", show_legend=True)

list_of_series = []
for model_index in range(number_of_models):
    list_of_series.append({"data": accuracy_dl[model_index], "label": "HA + CNN", "color": "grey", "legend": False})
    list_of_series.append({"data": accuracy_ha[model_index], "label": "HA", "color": "orange", "legend": False})
plots.create_line_plot(1, list_of_series, "Single Traces", "Accuracy", show_legend=True)

plots.show_plot()

backend.clear_session()
