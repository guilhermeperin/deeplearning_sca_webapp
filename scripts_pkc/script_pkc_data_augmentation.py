import numpy as np
from keras import backend as K
from deep_learning_models.deeplearning import Model
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import TrainingCallback, ValidationCallback
from commons.sca_datasets import ScaDataSets
from commons.sca_plots import Plots
import random

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'C:/Users/Guilherme/Inspector/data/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("ecc")

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#

data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, y_train, y_validation, x_train_d, x_validation_d = data_sets.load_training_and_validation_pkc()

# ---------------------------------------------------------------------------------------------------------------------#
#  Declare callbacks
# ---------------------------------------------------------------------------------------------------------------------#

callbacks_training = TrainingCallback(x_train, y_train)
callbacks_validation = ValidationCallback(x_validation, y_validation)


# ---------------------------------------------------------------------------------------------------------------------#
#  Function for Data Augmentation
#  Description: shifts the traces, creating artificial misalignment
# ---------------------------------------------------------------------------------------------------------------------#

def data_augmentation(data_set_samples, data_set_labels):
    ns = param["number_of_samples"]

    while True:
        x_train_shifted = np.zeros((len(data_set_samples), ns))

        for trace_index in range(len(data_set_samples)):

            shift = random.randint(-50, 50)

            if shift > 0:
                x_train_shifted[trace_index][0:ns - shift] = data_set_samples[trace_index][shift:ns]
                x_train_shifted[trace_index][ns - shift:ns] = data_set_samples[trace_index][0:shift]
            else:
                x_train_shifted[trace_index][0:abs(shift)] = data_set_samples[trace_index][ns - abs(shift):ns]
                x_train_shifted[trace_index][abs(shift):ns] = data_set_samples[trace_index][0:ns - abs(shift)]

        yield (x_train_shifted.reshape((x_train_shifted.shape[0], x_train_shifted.shape[1], 1)), data_set_labels)


# ---------------------------------------------------------------------------------------------------------------------#
#  Declare neural network model and run training by calling .fit()
# ---------------------------------------------------------------------------------------------------------------------#

model_obj = Model()
model = model_obj.basic_mlp(param["classes"], param["number_of_samples"])

model.fit_generator(
    generator=data_augmentation(x_train_d, y_train),
    steps_per_epoch=100,
    epochs=param["epochs"],
    validation_data=data_augmentation(x_validation_d, y_validation),
    validation_steps=100,
    callbacks=[callbacks_training, callbacks_validation]
)

# ---------------------------------------------------------------------------------------------------------------------#
#  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
# ---------------------------------------------------------------------------------------------------------------------#

accuracy_training = callbacks_training.get_accuracy()
accuracy_validation = callbacks_validation.get_accuracy()

recall_training = callbacks_training.get_recall()
recall_validation = callbacks_validation.get_recall()

loss_training = callbacks_training.get_loss()
loss_validation = callbacks_validation.get_loss()

# ---------------------------------------------------------------------------------------------------------------------#
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(1, 1)
plots.new_plot()
list_of_series = [
    {
        "data": accuracy_training,
        "label": "Training",
        "color": "blue",
        "legend": True
    },
    {
        "data": accuracy_validation,
        "label": "Validation",
        "color": "orange",
        "legend": True
    }
]
plots.create_line_plot(1, list_of_series, "Epochs", "Accuracy", show_legend=True)

plots.show_plot()

K.clear_session()
