from keras import backend
from keras.utils import to_categorical
from deep_learning_models.deeplearning import DeepLearningModel
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import TrainingCallback, ValidationCallback, CalculateAccuracyCurve25519
from commons.sca_datasets import ScaDataSets
from commons.sca_plots import Plots

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'D:/traces/'
trs_parameters = ScaParameters()
param = trs_parameters.get_trace_set("ecc_ha")

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#

data_sets = ScaDataSets(param, trace_directory_path)
x_train, _, y_train_data, _, _, _ = data_sets.load_training_and_validation_sets()
x_validation, y_validation_data, _ = data_sets.load_test_set()

# ---------------------------------------------------------------------------------------------------------------------#
#  Get trace data/labels as categorical values (e.g., label 0 = [1, 0], label 1 = [0, 1])
#  traces data contains two bytes:
#  byte 0: labels obtained after applying an horizontal attack
#  byte 1: true labels from correct key
# ---------------------------------------------------------------------------------------------------------------------#

labels_from_pkc_data_train_ha = [0 if row[0] == 0 else 1 for row in y_train_data]
labels_from_pkc_data_train_true = [0 if row[1] == 0 else 1 for row in y_train_data]
labels_from_pkc_data_validation_ha = [0 if row[0] == 0 else 1 for row in y_validation_data]
labels_from_pkc_data_validation_true = [0 if row[1] == 0 else 1 for row in y_validation_data]

y_train = to_categorical(labels_from_pkc_data_train_ha, num_classes=param["classes"])
y_validation = to_categorical(labels_from_pkc_data_validation_true, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Get amount of correct bits from HA labels
# ---------------------------------------------------------------------------------------------------------------------#

correct_bits_training_set = 0
for index in range(len(labels_from_pkc_data_train_true)):
    if labels_from_pkc_data_train_true[index] == labels_from_pkc_data_train_ha[index]:
        correct_bits_training_set += 1

correct_bits_validation_set = 0
for index in range(len(labels_from_pkc_data_validation_true)):
    if labels_from_pkc_data_validation_true[index] == labels_from_pkc_data_validation_ha[index]:
        correct_bits_validation_set += 1

print("Correct rate for training set: " + str(correct_bits_training_set / len(labels_from_pkc_data_train_true)))
print(
    "Correct rate for validation set: " + str(correct_bits_validation_set / len(labels_from_pkc_data_validation_true)))

# ---------------------------------------------------------------------------------------------------------------------#
#  Declare callbacks
# ---------------------------------------------------------------------------------------------------------------------#

callbacks_training = TrainingCallback(x_train, y_train)
callbacks_validation = ValidationCallback(x_validation, y_validation)
callbacks_pkc_curve25519 = CalculateAccuracyCurve25519(x_validation, labels_from_pkc_data_validation_true,
                                                       labels_from_pkc_data_validation_ha, param["epochs"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Declare neural network model and run training by calling .fit()
# ---------------------------------------------------------------------------------------------------------------------#

model_obj = DeepLearningModel()
model = model_obj.basic_cnn(param["classes"], param["number_of_samples"])
model.fit(x=x_train,
          y=y_train,
          batch_size=128,
          verbose=1,
          epochs=param["epochs"],
          shuffle=True,
          validation_data=(x_validation, y_validation),
          callbacks=[callbacks_training, callbacks_validation, callbacks_pkc_curve25519])

# ---------------------------------------------------------------------------------------------------------------------#
#  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
# ---------------------------------------------------------------------------------------------------------------------#

accuracy_training = callbacks_training.get_accuracy()
accuracy_validation = callbacks_validation.get_accuracy()

recall_training = callbacks_training.get_recall()
recall_validation = callbacks_validation.get_recall()

loss_training = callbacks_training.get_loss()
loss_validation = callbacks_validation.get_loss()

accuracy_dl = callbacks_pkc_curve25519.get_correct_dl()
accuracy_ha = callbacks_pkc_curve25519.get_correct_ha()

# ---------------------------------------------------------------------------------------------------------------------#
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(1, 2)
plots.new_plot()
list_of_series = [
    {"data": accuracy_training, "label": "Training", "color": "blue", "legend": True},
    {"data": accuracy_validation, "label": "Validation", "color": "orange", "legend": True}
]
plots.create_line_plot(1, list_of_series, "Epochs", "Accuracy", show_legend=True)

list_of_series = [
    {"data": accuracy_dl, "label": "HA + CNN", "color": "blue", "legend": True},
    {"data": accuracy_ha, "label": "HA", "color": "orange", "legend": True}
]
plots.create_line_plot(2, list_of_series, "Traces", "Accuracy", show_legend=True)

plots.show_plot()

backend.clear_session()
